import 'package:flutter/material.dart';

class EmpresFieldContainer extends StatefulWidget {
  final Widget child;
  final Color color;
  final EdgeInsets margin;

  EmpresFieldContainer({Key key, this.color, @required this.child, this.margin})
      : super(key: key);

  @override
  _EmpresFieldContainerState createState() => _EmpresFieldContainerState();
}

class _EmpresFieldContainerState extends State<EmpresFieldContainer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: 300,
//      height: 50,
      margin: widget.margin,
      color: widget.color,
      child: widget.child,
    );
  }
}
