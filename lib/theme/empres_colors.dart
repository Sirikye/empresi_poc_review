import 'package:flutter/material.dart';

class EmpresColors {
  /// Primary Colors
  static const Color primaryColor = const Color(0xFF5b92e5);
  static const Color primaryColorDark = const Color(0xFF1665b2);
  static const Color accentColor = const Color(0xFF6264A7);
  static const Color primaryLightColor = const Color(0xFF91c2ff);
  static const Color textFieldColor = const Color(0xFFCCCCCC);
  static const Color backgroundColor = const Color(0xFFE5E5E5);
//  static const Color secondaryColor = const Color(0xFFf9ba00);
//  static const Color secondaryLightColor = const Color(0xFFffec4d);
//  static const Color secondaryDarkColor = const Color(0xFFc18a00);
//  static const Color primaryTextColor = const Color(0xFFc18a00);
//  static const Color secondaryTextColor = const Color(0xFF00235f);

  /// TextInput Colors
  static const Color asteriskColor = const Color(0xFFF44336);
  static const Color inputCursorColor = const Color(0xFF6264A7);
  static const Color focusedInputColor = const Color(0xFFffc400);
  static const Color enabledInputColor = const Color(0xFF6264A7);
  static const Color hintTextColor = const Color(0xFF6264A7);
  static const Color labelTextColor = const Color.fromARGB(255, 93, 85, 85);
  static const Color textInputFillColor = const Color(0xFFEEEEEE);
  static const Color textInputFocusColor = const Color(0xFFffc400);
  static const Color textInputSuffixIconColor = const Color(0xFF6264A7);
}
