import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class ControlMeasuresWidget extends StatefulWidget {
  @override
  _ControlMeasuresWidgetState createState() => _ControlMeasuresWidgetState();
}

class _ControlMeasuresWidgetState extends State<ControlMeasuresWidget> {
  var _controlMeasuresFormKey = GlobalKey<FormState>();
  Locale locale;

  TextEditingController atRiskController = TextEditingController();
  TextEditingController casesController = TextEditingController();
  TextEditingController deathsController = TextEditingController();
  TextEditingController destroyedController = TextEditingController();
  TextEditingController slaughteredController = TextEditingController();
  TextEditingController vaccinationController = TextEditingController();

  /// List for Data columns
  List<String> dataColumns = [
    'Animal Type',
    'Animal Class',
    'Species',
    'At Risk',
    'Cases',
    'Deaths',
    'Destroyed',
    'Vaccinated',
    'Vaccination',
    'Production System',
  ];
  var selectedSpeciesInfoList = [
    'Domestic',
    'Mammal',
    'Cattle',
    '1000',
    '500',
    '1'
        '00',
    '20',
    'Yes',
    'Pneumonia',
    'Commercial'
  ];

  /// List for Clinical Signs columns
  List<String> clinicalSignsColumns = [
    'Clinical Sign',
    'Refer To',
  ];

  /// List for Lesion columns
  List<String> lesionColumns = [
    'Lesion',
    'Refer To',
  ];

//  List<Classification> selectedSpeciesInfoList = List<Classification>();
//
//  List<ClinicalSign> clinicalSignList = List<ClinicalSign>();
//  List<ClinicalSign> selectedClinicalSigns;
//  List<Lesion> lesionList = List<Lesion>();
//  List<Lesion> selectedLesions;
//  List<Classification> selectedSpeciesInfo;
//
//  AnimalClass selectedAnimalClass;
//  AnimalType selectedAnimalType;
//  Species selectedAnimalSpecies;
//  ProductionSystemOne selectedAnimalProductionSystemOne;
//  ProductionSystemTwo selectedAnimalProductionSystemTwo;

  String selectedVaccinationOption;

  String systemOne = '-';
  String systemTwo = '-';

  String selectedClinicalSign;
  String selectedLesion;

  String clinicalIsHuman = 'No';
  String lesionIsHuman = 'No';

  String clinicalSpecies;
  String lesionSpecies;

//  List<AnimalType> animalTypeList;
//  List<AnimalClass> animalClassList;
//  List<Species> animalSpeciesList;

  List<DropdownMenuItem> animalTypeList = [
    DropdownMenuItem(
      child: Text('Domestic'),
    ),
    DropdownMenuItem(
      child: Text('Wild'),
    ),
    DropdownMenuItem(
      child: Text('Captive'),
    ),
  ];

  List<DropdownMenuItem> animalClassList = [
    DropdownMenuItem(
      child: Text('Birds'),
    ),
    DropdownMenuItem(
      child: Text('Anthropods'),
    ),
  ];

  List<DropdownMenuItem> animalSpeciesList = [
    DropdownMenuItem(
      child: Text('Cattle'),
    ),
    DropdownMenuItem(
      child: Text('Chicken'),
    ),
    DropdownMenuItem(
      child: Text('Bees'),
    ),
  ];

  String selectedAnimalTypeId;
  int selectedAnimalClassId;
  int selectedAnimalSpeciesId;

//  Set<ProductionSystemOne> productSystemOneList;
//  List<ProductionSystemTwo> productSystemTwoList;

  int productionSystemOneId;

  var selectedSpeciesInfo;

  var clinicalSignList = ['Swollen Eye', 'Swollen Tongue'];

  var selectedClinicalSigns;

  var lesionList = ['Blood in faeces', 'Reduced laction'];

  var selectedLesions;

  void showToast(String message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
          child: Builder(
            builder: (context) => Form(
              key: _controlMeasuresFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Control Measures Information',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Animal Type',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 5.0,
                  ),
//                StreamBuilder<QuerySnapshot>(
//                  stream: repository.getAnimalTypes(typeList ?? ['0']),
//                  builder: (context, snapshots) {
//                    if (!snapshots.hasData) {
//                      return CircularProgressIndicator();
//                    }
//                    var typesData = snapshots.data.documents;
//                    var code = locale.languageCode;
//                    animalTypeList = typesData
//                        .map((snapshot) =>
//                            AnimalType.animalTypeFromFireStore(snapshot))
//                        .toList();
//                    return Padding(
//                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
//                      child: Container(
//                        color: EmpresColors.textInputFillColor,
//                        child: DropdownButtonHideUnderline(
//                          child: DropdownButton(
//                            hint: Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Text('Choose Animal Type'),
//                            ),
//                            isExpanded: true,
//                            items: animalTypeList
//                                .map((animalType) => DropdownMenuItem(
//                                    value: animalType,
//                                    child: Padding(
//                                      padding: const EdgeInsets.all(8.0),
//                                      child: Text(animalType.typeEn),
//                                    )))
//                                .toList(),
//                            onChanged: (value) {
//                              setState(
//                                () {
//                                  selectedAnimalType = value;
//                                  selectedAnimalTypeId =
//                                      selectedAnimalType.typeId;
//                                },
//                              );
//                            },
//                            value: selectedAnimalType,
//                          ),
//                        ),
//                      ),
//                    );
//                  },
//                ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Animal Type'),
                          hint: Text('Animal Type'),
                          items: animalTypeList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Animal Class'),
                          hint: Text('Animal Class'),
                          items: animalClassList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Animal Species'),
                          hint: Text('Animal Species'),
                          items: animalSpeciesList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'At Risk',
                            hintText: 'E.g 200',
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: atRiskController,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Cases',
                            hintText: 'E.g 100',
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: casesController,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Deaths',
                            hintText: 'E.g 50',
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: deathsController,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Destroyed',
                            hintText: 'E.g 25',
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: destroyedController,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
//
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Slaughtered',
                            hintText: 'E.g 25',
                            child: TextFormField(
                              keyboardType: TextInputType.number,
                              controller: slaughteredController,

//                            validator: (value) {
//                              if (value.isEmpty) {
//                                return 'Enter numbers of slaughtered';
//                              }
//                              return null;
//                            },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),

                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                    ),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Vaccination'),
                          hint: Text('Vaccination'),
                          items: [
                            DropdownMenuItem(
                              child: Text('Yes'),
                            ),
                            DropdownMenuItem(
                              child: Text('No'),
                            ),
                          ],
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            labelText: 'Vaccination',
                            hintText: 'Vaccination',
                            flexLeft: 1,
                            flexRight: 1,
                            child: TextFormField(
                              readOnly: selectedVaccinationOption == "Yes"
                                  ? false
                                  : true,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.sentences,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Production System 1'),
                          hint: Text('Production System 1'),
                          items: [
                            DropdownMenuItem(
                              child: Text('Commercial'),
                            ),
                            DropdownMenuItem(
                              child: Text('Small scale'),
                            ),
                          ],
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Production System 2'),
                          hint: Text('Production System 2'),
                          items: [
                            DropdownMenuItem(
                              child: Text('Goat raring'),
                            ),
                            DropdownMenuItem(
                              child: Text('Cattle raring'),
                            ),
                          ],
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: GenericButtonWithIcon(
                      icon: Icon(Icons.add_circle, color: Colors.white),
                      text: Text('Add Classification',
                          style: TextStyle(color: Colors.white)),
                      color: Colors.blue,
                      accentColor: Colors.red,
                      accentIcon: Icon(Icons.add_circle, color: Colors.black),
                      accentText: Text('Add Classification',
                          style: TextStyle(color: Colors.black)),
                      textPadding: EdgeInsets.only(left: 5),
                      iconPadding: EdgeInsets.all(5),
                      onTap: () {
                        print('tapped');
                      },
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Species and Control Measures',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Card(
                        elevation: 2.0,
                        color: EmpresColors.textInputFillColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0)),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: DataTable(
                                dividerThickness: 2.0,
                                columns: dataColumns.map((e) {
                                  return DataColumn(
                                      label: Text(
                                    e,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ));
                                }).toList(),
                                rows: [
                                  DataRow(
                                      cells: selectedSpeciesInfoList
                                          .map((e) => DataCell(Text(e)))
                                          .toList()),
                                ]),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Clinical Signs',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
//                                        onTap: () {openClinicalSignsDialog
//                                          (context);},
                              child: Text(
                                'Add New Clinical Signs',
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Card(
                        elevation: 2.0,
                        color: EmpresColors.textInputFillColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0)),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: DataTable(
                                dividerThickness: 2.0,
                                columns: clinicalSignsColumns.map((e) {
                                  return DataColumn(
                                      label: Flexible(
                                    fit: FlexFit.tight,
                                    child: Text(
                                      e,
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                  ));
                                }).toList(),
                                rows: [
                                  DataRow(
                                      cells: clinicalSignList
                                          .map((e) => DataCell(Text(e)))
                                          .toList()),
                                ]),
                          ),
                        ),
                      )
                    ],
                  )),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              'Lesions',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: InkWell(
//                                        onTap: () {openLesionDialog(context);},
                              child: Text(
                                'Add New Lesion',
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.w700,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Card(
                        elevation: 2.0,
                        color: EmpresColors.textInputFillColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0)),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: DataTable(
                                showCheckboxColumn: true,
                                dividerThickness: 2.0,
                                columns: lesionColumns.map((e) {
                                  return DataColumn(
                                      label: Text(
                                    e,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ));
                                }).toList(),
                                rows: [
                                  DataRow(
                                      cells: lesionList
                                          .map((e) => DataCell(Text(e)))
                                          .toList()),
                                ]),
                          ),
                        ),
                      )
                    ],
                  )),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save, color: Colors.white),
                          text: Text('Save Progress',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Save Progress',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.play_arrow, color: Colors.white),
                          text: Text('Next',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Next',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

//  void openClinicalSignsDialog(BuildContext context) {
//    var eventProvider = Provider.of<EventProvider>(context, listen: false);
//    List<String> speciesList = [
//      '${eventProvider.species == null ? 'No '
//          'Species Selected' : eventProvider.species}'
//    ];
////    clinicalIsHuman = eventProvider.humanReference;
//    showDialog(
//        context: context,
//        child: Center(
//          child: Container(
//            height: 450,
//            width: 400,
//            child: Dialog(
//              elevation: 3.0,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(15.0)),
//              child: StatefulBuilder(
//                builder: (context, setDialogState) => Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: [
//                    Container(
//                        padding: EdgeInsets.symmetric(
//                            horizontal: 12.0, vertical: 12.0),
//                        alignment: Alignment.centerLeft,
//                        decoration: BoxDecoration(
//                          color: Colors.grey[300],
//                          borderRadius: new BorderRadius.only(
//                              topLeft: const Radius.circular(15),
//                              topRight: const Radius.circular(15)),
//                        ),
//                        child: Text('Insert New Clinical Sign')),
//                    SizedBox(
//                      height: 20.0,
//                    ),
//                    Flexible(
//                      child: Container(
//                        height: 400,
//                        width: 400,
//                        child: ListView(
//                          children: [
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: Text('Selected Species'),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: EmpresFieldContainer(
//                                color: EmpresColors.textInputFillColor,
//                                child: DropdownButtonHideUnderline(
//                                  child: DropdownButton(
//                                    isExpanded: true,
//                                    hint: Padding(
//                                      padding: const EdgeInsets.all(8.0),
//                                      child: Text('Choose Species Selected'),
//                                    ),
//                                    items: clinicalIsHuman == 'Human'
//                                        ? null
//                                        : speciesList
//                                            .map((item) => DropdownMenuItem(
//                                                value: item,
//                                                child: Padding(
//                                                  padding:
//                                                      const EdgeInsets.all(8.0),
//                                                  child: Text(item),
//                                                )))
//                                            .toList(),
//                                    onChanged: (value) {
//                                      setDialogState(() {
//                                        clinicalSpecies = value;
//                                      });
//                                    },
//                                    value: clinicalSpecies,
//                                  ),
//                                ),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 4.0, vertical: 5.0),
//                              child: EmpresSwitch(
//                                  switchTitle: Text('Is Human'),
//                                  onSwitchValueChanged: (value) {
//                                    if (value == true) {
//                                      clinicalIsHuman = 'Human';
////                                      eventProvider
////                                          .setReferToHuman(clinicalIsHuman);
//                                    } else {
//                                      clinicalIsHuman = 'No';
////                                      eventProvider
////                                          .setReferToHuman(clinicalIsHuman);
//                                    }
//                                  }),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: Text('Clinical Signs'),
//                            ),
//                            StreamBuilder<QuerySnapshot>(
//                                stream: repository.getClinicalSigns(),
//                                builder: (context, snapshot) {
//                                  if (!snapshot.hasData) {
//                                    return Text('Loading...');
//                                  }
//                                  var data = snapshot.data.documents;
//                                  return Padding(
//                                    padding: const EdgeInsets.only(
//                                        left: 12.0, right: 12.0, top: 5.0),
//                                    child: EmpresFieldContainer(
//                                      color: EmpresColors.textInputFillColor,
//                                      child: DropdownButtonHideUnderline(
//                                        child: DropdownButton(
//                                          hint: Text('Choose Clinical Sign'),
//                                          isExpanded: true,
//                                          items: data
//                                              .map((sign) => DropdownMenuItem(
//                                                  value: sign['sign_en'],
//                                                  child: Padding(
//                                                    padding:
//                                                        const EdgeInsets.all(
//                                                            8.0),
//                                                    child:
//                                                        Text(sign['sign_en']),
//                                                  )))
//                                              .toList(),
//                                          onChanged: (value) {
//                                            setDialogState(() {
//                                              selectedClinicalSign = value;
//                                            });
//                                          },
//                                          value: selectedClinicalSign,
//                                        ),
//                                      ),
//                                    ),
//                                  );
//                                }),
//                          ],
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: ButtonBar(
//                        children: <Widget>[
//                          FlatButton.icon(
//                            icon: Icon(Icons.save),
//                            label: Text('Save'),
//                            onPressed: () {
//                              setDialogState(() {
//                                ClinicalSign clinicalSign = ClinicalSign(
//                                    selectedClinicalSign, clinicalSpecies);
//
//                                if (eventProvider.clinicalSignList.every(
//                                    (sign) =>
//                                        sign?.sign != selectedClinicalSign)) {
//                                  eventProvider.saveClinicalSign(clinicalSign);
//                                  eventProvider
//                                      .addClinicalSignList(clinicalSign);
//                                  clinicalSignList =
//                                      eventProvider.clinicalSignList;
//                                  Navigator.pop(context);
//                                } else {
//                                  displayErrorDialog(
//                                      context,
//                                      'Clinical Sign '
//                                      'already exists');
//                                }
//                              });
//                            },
//                          ),
//                          FlatButton.icon(
//                            icon: Icon(Icons.close),
//                            label: Text('Close'),
//                            onPressed: () {
//                              Navigator.pop(context);
//                            },
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ));
//  }
//
//  void openLesionDialog(BuildContext context) {
//    var eventProvider = Provider.of<EventProvider>(context, listen: false);
//    List<String> speciesList = [
//      '${eventProvider.species == null ? 'No '
//          'Species Selected' : eventProvider.species}'
//    ];
//    showDialog(
//        context: context,
//        child: Center(
//          child: Container(
//            height: 450,
//            width: 400,
//            child: Dialog(
//              elevation: 3.0,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(15.0)),
//              child: StatefulBuilder(
//                builder: (context, setState) => Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: [
//                    Container(
//                        padding: EdgeInsets.all(12),
//                        alignment: Alignment.centerLeft,
//                        decoration: BoxDecoration(
//                          color: Colors.grey[300],
//                          borderRadius: new BorderRadius.only(
//                              topLeft: const Radius.circular(15),
//                              topRight: const Radius.circular(15)),
//                        ),
//                        child: Text('Insert New Lesion')),
//                    SizedBox(
//                      height: 20.0,
//                    ),
//                    Flexible(
//                      child: Container(
//                        height: 400,
//                        width: 400,
//                        child: ListView(
//                          children: [
//                            Padding(
//                              padding: const EdgeInsets.all(12.0),
//                              child: Text('Selected Species'),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: EmpresFieldContainer(
//                                color: EmpresColors.textInputFillColor,
//                                child: DropdownButtonHideUnderline(
//                                  child: DropdownButton(
//                                    isExpanded: true,
//                                    hint: Padding(
//                                      padding: const EdgeInsets.all(8.0),
//                                      child: Text('Choose Selected Species'),
//                                    ),
//                                    items: speciesList
//                                        .map((item) => DropdownMenuItem(
//                                              value: item,
//                                              child: Padding(
//                                                padding:
//                                                    const EdgeInsets.all(8.0),
//                                                child: Text(item),
//                                              ),
//                                            ))
//                                        .toList(),
//                                    onChanged: (value) {
//                                      setState(() {
//                                        lesionSpecies = value;
//                                      });
//                                    },
//                                    value: lesionSpecies,
//                                  ),
//                                ),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 4.0, vertical: 5.0),
//                              child: EmpresSwitch(
//                                  switchTitle: Text('Is Human'),
//                                  onSwitchValueChanged: (value) {
//                                    setState(() {
//                                      if (value == true) {
//                                        lesionIsHuman = 'Human';
//                                      } else {
//                                        lesionIsHuman = 'No';
//                                      }
//                                    });
//                                  }),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: Text('Lesion'),
//                            ),
//                            StreamBuilder<QuerySnapshot>(
//                                stream: repository.getLesions(),
//                                builder: (context, snapshot) {
//                                  if (!snapshot.hasData) {
//                                    return Text('Loading...');
//                                  }
//                                  var data = snapshot.data.documents;
//                                  return Padding(
//                                    padding: const EdgeInsets.all(12.0),
//                                    child: EmpresFieldContainer(
//                                      color: EmpresColors.textInputFillColor,
//                                      child: DropdownButtonHideUnderline(
//                                        child: DropdownButton(
//                                          hint: Padding(
//                                            padding: const EdgeInsets.all(8.0),
//                                            child: Text('Choose Lesion'),
//                                          ),
//                                          items: data
//                                              .map((lesion) => DropdownMenuItem(
//                                                    value: lesion['lesion_en'],
//                                                    child: Padding(
//                                                      padding:
//                                                          const EdgeInsets.all(
//                                                              8.0),
//                                                      child: Text(
//                                                          lesion['lesion_en']),
//                                                    ),
//                                                  ))
//                                              .toList(),
//                                          onChanged: (value) {
//                                            setState(() {
//                                              selectedLesion = value;
//                                            });
//                                          },
//                                          value: selectedLesion,
//                                        ),
//                                      ),
//                                    ),
//                                  );
//                                }),
//                          ],
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: ButtonBar(
//                        children: <Widget>[
//                          FlatButton.icon(
//                            icon: Icon(Icons.save),
//                            label: Text('Save'),
//                            onPressed: () {
//                              setState(() {
//                                Lesion lesion =
//                                    Lesion(selectedLesion, lesionSpecies);
//                                if (lesionList.every((lesion) =>
//                                    lesion.lesion != selectedLesion)) {
//                                  lesionList.add(lesion);
//                                  eventProvider.addLesionList(lesionList);
//                                  Navigator.pop(context);
//                                } else {
//                                  displayErrorDialog(
//                                      context,
//                                      'Lesion already'
//                                      ' exists');
//                                }
//                              });
//                            },
//                          ),
//                          FlatButton.icon(
//                            icon: Icon(Icons.close),
//                            label: Text('Close'),
//                            onPressed: () {
//                              Navigator.pop(context);
//                            },
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ));
//  }
//
//  void editClinicalSignsDialog(BuildContext context, ClinicalSign clinicalSign, int index) {
//    var eventProvider = Provider.of<EventProvider>(context, listen: false);
//    List<String> speciesList = [
//      '${eventProvider.species == null ? 'No '
//          'Species Selected' : eventProvider.species}'
//    ];
//    print('Selected Sign:${clinicalSign.sign}');
//    print('Selected Sign Index:$index');
//    String currentSign;
//    showDialog(
//        context: context,
//        child: Center(
//          child: Container(
//            height: 450,
//            width: 400,
//            child: Dialog(
//              elevation: 3.0,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(15.0)),
//              child: StatefulBuilder(
//                builder: (context, setDialogState) => Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: [
//                    Container(
//                        padding: EdgeInsets.symmetric(
//                            horizontal: 12.0, vertical: 12.0),
//                        alignment: Alignment.centerLeft,
//                        decoration: BoxDecoration(
//                          color: Colors.grey[300],
//                          borderRadius: new BorderRadius.only(
//                              topLeft: const Radius.circular(15),
//                              topRight: const Radius.circular(15)),
//                        ),
//                        child: Text('Edit Clinical Sign')),
//                    SizedBox(
//                      height: 20.0,
//                    ),
//                    Flexible(
//                      child: Container(
//                        height: 400,
//                        width: 400,
//                        child: ListView(
//                          children: [
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: Text('Selected Species'),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: EmpresFieldContainer(
//                                color: EmpresColors.textInputFillColor,
//                                child: DropdownButtonHideUnderline(
//                                  child: DropdownButton(
//                                    isExpanded: true,
//                                    hint: Padding(
//                                      padding: const EdgeInsets.all(8.0),
//                                      child: Text('Choose Species Selected'),
//                                    ),
//                                    items: speciesList
//                                        .map((item) => DropdownMenuItem(
//                                            value: item,
//                                            child: Padding(
//                                              padding:
//                                                  const EdgeInsets.all(8.0),
//                                              child: Text(item),
//                                            )))
//                                        .toList(),
//                                    onChanged: (value) {
//                                      setDialogState(() {
//                                        clinicalSpecies = value;
//                                      });
//                                    },
//                                    value: clinicalSpecies,
//                                  ),
//                                ),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 4.0, vertical: 5.0),
//                              child: EmpresSwitch(
//                                  switchTitle: Text('Is Human'),
//                                  onSwitchValueChanged: (value) {
//                                    if (value == true) {
//                                      clinicalIsHuman = 'Human';
//                                    } else {
//                                      clinicalIsHuman = 'No';
//                                    }
//                                  }),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: Text('Clinical Signs'),
//                            ),
//                            StreamBuilder<QuerySnapshot>(
//                                stream: repository.getClinicalSigns(),
//                                builder: (context, snapshot) {
//                                  if (!snapshot.hasData) {
//                                    return Text('Loading...');
//                                  }
//                                  var data = snapshot.data.documents;
//                                  return Padding(
//                                    padding: const EdgeInsets.only(
//                                        left: 12.0, right: 12.0, top: 5.0),
//                                    child: EmpresFieldContainer(
//                                      color: EmpresColors.textInputFillColor,
//                                      child: DropdownButtonHideUnderline(
//                                        child: DropdownButton(
//                                          hint: Text('Choose Clinical Sign'),
//                                          isExpanded: true,
//                                          items: data
//                                              .map((sign) => DropdownMenuItem(
//                                                  value: sign['sign_en'],
//                                                  child: Padding(
//                                                    padding:
//                                                        const EdgeInsets.all(
//                                                            8.0),
//                                                    child:
//                                                        Text(sign['sign_en']),
//                                                  )))
//                                              .toList(),
//                                          onChanged: (value) {
//                                            setDialogState(() {
//                                              currentSign = value;
//                                            });
//                                          },
//                                          value: currentSign,
//                                        ),
//                                      ),
//                                    ),
//                                  );
//                                }),
//                          ],
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: ButtonBar(
//                        children: <Widget>[
//                          FlatButton.icon(
//                            icon: Icon(Icons.save),
//                            label: Text('Update'),
//                            onPressed: () {
//                              setDialogState(() {
//                                ClinicalSign newClinicalSign =
//                                    ClinicalSign(currentSign, clinicalSpecies);
//
//                                if (eventProvider.clinicalSignList.every(
//                                    (sign) => sign?.sign != currentSign)) {
////                                  clinicalSignList.replaceRange(
////                                      index, index, [newClinicalSign]);
//                                  clinicalSignList[index] = newClinicalSign;
//                                  clinicalSignList =
//                                      eventProvider.clinicalSignList;
//                                  Navigator.pop(context);
//                                } else {
//                                  displayErrorDialog(
//                                      context,
//                                      'Clinical Sign '
//                                      'already exists');
//                                }
//                              });
//                            },
//                          ),
//                          FlatButton.icon(
//                            icon: Icon(Icons.close),
//                            label: Text('Close'),
//                            onPressed: () {
//                              Navigator.pop(context);
//                            },
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ));
//  }
//
//  void editLesionDialog(BuildContext context, Lesion lesion, int index) {
//    var eventProvider = Provider.of<EventProvider>(context, listen: false);
//    List<String> speciesList = [
//      '${eventProvider.species == null ? 'No '
//          'Species Selected' : eventProvider.species}'
//    ];
//    print('Lesion to Edit:${lesion.lesion}');
//    print('Lesion Index to Edit:$index');
//    String currentLesion;
//    showDialog(
//        context: context,
//        child: Center(
//          child: Container(
//            height: 450,
//            width: 400,
//            child: Dialog(
//              elevation: 3.0,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(15.0)),
//              child: StatefulBuilder(
//                builder: (context, setState) => Column(
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  mainAxisAlignment: MainAxisAlignment.start,
//                  children: [
//                    Container(
//                        padding: EdgeInsets.all(12),
//                        alignment: Alignment.centerLeft,
//                        decoration: BoxDecoration(
//                          color: Colors.grey[300],
//                          borderRadius: new BorderRadius.only(
//                              topLeft: const Radius.circular(15),
//                              topRight: const Radius.circular(15)),
//                        ),
//                        child: Text('Edit Lesion')),
//                    SizedBox(
//                      height: 20.0,
//                    ),
//                    Flexible(
//                      child: Container(
//                        height: 400,
//                        width: 400,
//                        child: ListView(
//                          children: [
//                            Padding(
//                              padding: const EdgeInsets.all(12.0),
//                              child: Text('Selected Species'),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: EmpresFieldContainer(
//                                color: EmpresColors.textInputFillColor,
//                                child: DropdownButtonHideUnderline(
//                                  child: DropdownButton(
//                                    isExpanded: true,
//                                    hint: Padding(
//                                      padding: const EdgeInsets.all(8.0),
//                                      child: Text('Choose Selected Species'),
//                                    ),
//                                    items: speciesList
//                                        .map((item) => DropdownMenuItem(
//                                              value: item,
//                                              child: Padding(
//                                                padding:
//                                                    const EdgeInsets.all(8.0),
//                                                child: Text(item),
//                                              ),
//                                            ))
//                                        .toList(),
//                                    onChanged: (value) {
//                                      setState(() {
//                                        lesionSpecies = value;
//                                      });
//                                    },
//                                    value: lesionSpecies,
//                                  ),
//                                ),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 4.0, vertical: 5.0),
//                              child: EmpresSwitch(
//                                  switchTitle: Text('Is Human'),
//                                  onSwitchValueChanged: (value) {
//                                    setState(() {
//                                      if (value == true) {
//                                        lesionIsHuman = 'Human';
//                                      } else {
//                                        lesionIsHuman = 'No';
//                                      }
//                                    });
//                                  }),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.symmetric(
//                                  horizontal: 12.0, vertical: 5.0),
//                              child: Text('Lesion'),
//                            ),
//                            StreamBuilder<QuerySnapshot>(
//                                stream: repository.getLesions(),
//                                builder: (context, snapshot) {
//                                  if (!snapshot.hasData) {
//                                    return Text('Loading...');
//                                  }
//                                  var data = snapshot.data.documents;
//                                  return Padding(
//                                    padding: const EdgeInsets.all(12.0),
//                                    child: EmpresFieldContainer(
//                                      color: EmpresColors.textInputFillColor,
//                                      child: DropdownButtonHideUnderline(
//                                        child: DropdownButton(
//                                          hint: Padding(
//                                            padding: const EdgeInsets.all(8.0),
//                                            child: Text('Choose Lesion'),
//                                          ),
//                                          items: data
//                                              .map((lesion) => DropdownMenuItem(
//                                                    value: lesion['lesion_en'],
//                                                    child: Padding(
//                                                      padding:
//                                                          const EdgeInsets.all(
//                                                              8.0),
//                                                      child: Text(
//                                                          lesion['lesion_en']),
//                                                    ),
//                                                  ))
//                                              .toList(),
//                                          onChanged: (value) {
//                                            setState(() {
//                                              selectedLesion = value;
//                                            });
//                                          },
//                                          value: selectedLesion,
//                                        ),
//                                      ),
//                                    ),
//                                  );
//                                }),
//                          ],
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: ButtonBar(
//                        children: <Widget>[
//                          FlatButton.icon(
//                            icon: Icon(Icons.save),
//                            label: Text('Update'),
//                            onPressed: () {
//                              setState(() {
//                                Lesion newLesion =
//                                    Lesion(currentLesion, lesionSpecies);
//                                if (lesionList.every((lesion) =>
//                                    lesion.lesion != selectedLesion)) {
//                                  lesionList[index] = newLesion;
//                                  lesionList = eventProvider.lesionList;
//                                  Navigator.pop(context);
//                                } else {
//                                  displayErrorDialog(
//                                      context,
//                                      'Lesion already'
//                                      ' exists');
//                                }
//                              });
//                            },
//                          ),
//                          FlatButton.icon(
//                            icon: Icon(Icons.close),
//                            label: Text('Close'),
//                            onPressed: () {
//                              Navigator.pop(context);
//                            },
//                          ),
//                        ],
//                      ),
//                    ),
//                  ],
//                ),
//              ),
//            ),
//          ),
//        ));
//  }
//
//  displayErrorDialog(BuildContext context, String message) {
//    showDialog(
//        context: context,
//        builder: (BuildContext context) {
//          return AlertDialog(
//            shape: RoundedRectangleBorder(
//                borderRadius: BorderRadius.circular(10.0)),
//            title: Text("Error Message"),
//            content: Text(message),
//            actions: [
//              FlatButton(
//                child: Text("Ok"),
//                onPressed: () {
//                  Navigator.of(context).pop();
//                },
//              )
//            ],
//          );
//        });
//  }
//
//  void editClassificationInformation(int classificationIndex, Classification classification) {
//    setState(() {
//      atRiskController.text = classification?.animalsAtRisk ?? "0";
//    });
//  }
}
