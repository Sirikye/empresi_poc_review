import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';

class BootstrapStyledTextfield extends StatefulWidget {
  final double paddingTop;
  final double paddingRight;
  final int containerWidth;
  final String hintText;
  final TextEditingController controller;

  BootstrapStyledTextfield(
      {Key key,
      @required this.paddingTop = 30,
      @required this.paddingRight = 20,
      @required this.containerWidth = 400,
      @required this.hintText = 'Put your hint here',
      this.controller})
      : super(key: key);

  @override
  _BootstrapStyledTextfieldState createState() =>
      _BootstrapStyledTextfieldState();
}

class _BootstrapStyledTextfieldState extends State<BootstrapStyledTextfield> {
  @override
  Widget build(BuildContext context) {
    return Container(
        //width: 400,
        padding:
            EdgeInsets.only(top: widget.paddingTop, right: widget.paddingRight),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(5),
          child: Container(
            height: 100,
            width: 300,
            decoration: BoxDecoration(
              border: Border.all(color: EmpresColors.textFieldColor),
              borderRadius: BorderRadius.circular(5),
              boxShadow: [
                BoxShadow(
                  blurRadius: 10,
                  spreadRadius: 1,
                  offset: Offset(2, 6),
                  color: Colors.white70,
                ),
              ],
            ),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: TextField(
                    keyboardType: TextInputType.multiline,
                    minLines: 1,
                    maxLines: 6,
                    style: TextStyle(fontSize: 12),
                    controller: widget.controller,
                    decoration: InputDecoration(
                      hintText: widget.hintText,
                      //hintStyle: TextStyle(color: Colors.black),
                      contentPadding: EdgeInsets.only(left: 10, top: 10.0),
                      isDense: true,
                      alignLabelWithHint: true,
                      hintStyle: TextStyle(fontFamily: 'IBMPlexSans'),
                      border: InputBorder.none,
                      focusedBorder: InputBorder.none,
                      enabledBorder: InputBorder.none,
                      errorBorder: InputBorder.none,
                      disabledBorder: InputBorder.none,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
