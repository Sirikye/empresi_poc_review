import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/bootstrap_styled_textfield.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/empres_switch.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class EpidemiologyInformationWidget extends StatefulWidget {
  @override
  EpidemiologyInformationWidgetState createState() =>
      EpidemiologyInformationWidgetState();
}

class EpidemiologyInformationWidgetState
    extends State<EpidemiologyInformationWidget> {
  GlobalKey<FormState> _epidemiologyFormKey = GlobalKey<FormState>();

  TextEditingController humansAffectedController = TextEditingController();
  TextEditingController humanDeathsController = TextEditingController();
  TextEditingController ageController = TextEditingController();
  TextEditingController humanCaseCommentController = TextEditingController();
  TextEditingController epidemiologyCommentController = TextEditingController();

  bool _selectedHumanAffectedValue = false;

  List<String> selectedControlMeasures = [];
  List<String> selectedTreatments = [];
  List<String> selectedInfectionSources = [];

  String selectedSurveillanceType;
  String humanAffectedValue;
  String selectedGender;

  List<DropdownMenuItem> genderList = [
    DropdownMenuItem(
      child: Text('Male'),
    ),
    DropdownMenuItem(
      child: Text('Female'),
    ),
  ];
  List<DropdownMenuItem> surveillanceTypeList = [
    DropdownMenuItem(
      child: Text('Active'),
    ),
    DropdownMenuItem(
      child: Text('Passive'),
    ),
  ];
  List<String> treatmentList = [
    'Acaricide',
    'Anthelmintic',
  ];

  List<String> infectionSourceList = [
    'Animal/dog bite',
    'Airborne spread',
  ];

  List<String> controlMeasureList = [
    'Ban on import of live animals and meat.',
    'Quarantine',
  ];

  void humanAffectedOption(bool value) {}

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
          child: Builder(
            builder: (context) => Form(
              key: _epidemiologyFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Epidemiology Information',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Zoonosis',
                      style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14.0,
                          fontStyle: FontStyle.normal),
                    ),
                  ),
                  EmpresSwitch(
                    switchTitle: Text('Humans Affected'),
                    onSwitchValueChanged: (value) {
                      setState(() {
                        _selectedHumanAffectedValue = value;
                        if (_selectedHumanAffectedValue == true) {
                          humanAffectedValue = 'Yes';
                        } else {
                          humanAffectedValue = 'No';
                        }
                        print('Value changed to:$humanAffectedValue');
                      });
                    },
                  ),
                  _selectedHumanAffectedValue == false
                      ? SizedBox()
                      : SizedBox(
                          height: 10.0,
                        ),
                  _selectedHumanAffectedValue == false
                      ? SizedBox()
                      : Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              SizedBox(
                                width: 300,
                                child: BootstrapStyledInputBox(
                                  flexLeft: 1,
                                  flexRight: 1,
                                  labelText: 'Number of humans affected',
                                  hintText: 'E.g 1000',
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    controller: humansAffectedController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 300,
                                child: BootstrapStyledInputBox(
                                  flexRight: 1,
                                  flexLeft: 1,
                                  labelText: 'Number of human deaths',
                                  hintText: 'E.g 20',
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    controller: humanDeathsController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                  SizedBox(
                    height: 10.0,
                  ),
                  _selectedHumanAffectedValue == false
                      ? SizedBox()
                      : Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              DropdownWidget(
                                accentColor: EmpresColors.textFieldColor,
                                primaryColor: EmpresColors.textFieldColor,
                                width: 300,
                                label: Text('Gender'),
                                hint: Text('Gender'),
                                items: genderList,
                                onChanged: (value) {},
                                boxTextstyle: TextStyle(
                                    fontFamily: 'IBMPlexSans',
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: Color.fromARGB(255, 93, 85, 85)),
                                style: TextStyle(
                                    fontFamily: 'IBMPlexSans',
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: Color.fromARGB(255, 93, 85, 85)),
                                accentTextstyle: TextStyle(
                                    fontFamily: 'IBMPlexSans',
                                    fontSize: 12,
                                    fontWeight: FontWeight.normal,
                                    color: Colors.white),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              SizedBox(
                                width: 300,
                                child: BootstrapStyledInputBox(
                                  labelText: 'Age',
                                  hintText: 'Age',
                                  flexLeft: 1,
                                  flexRight: 1,
                                  child: TextFormField(
                                    keyboardType: TextInputType.number,
                                    controller: ageController,
                                    decoration: InputDecoration(
                                        border: InputBorder.none),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                  SizedBox(
                    height: 10.0,
                  ),
                  _selectedHumanAffectedValue == false
                      ? SizedBox()
                      : Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: SizedBox(
                            width: 300,
                            child: BootstrapStyledTextfield(
                              paddingRight: 8.0,
                              paddingTop: 8.0,
                              containerWidth: 300,
                              controller: humanCaseCommentController,
                              hintText: 'Comments for human cases',
                            ),
                          ),
                        ),
                  _selectedHumanAffectedValue == false
                      ? SizedBox()
                      : SizedBox(
                          height: 10.0,
                        ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              'Control Measures',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            InkWell(
                                onTap: () => openControlMeasuresDialog(context),
                                child: Text(
                                  '<- Click To Select',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 300,
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(2)),
                              child: selectedControlMeasures.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('No data to display'),
                                    )
                                  : ListView(
                                      shrinkWrap: true,
                                      children: selectedControlMeasures
                                          .map((measure) => Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Text(measure),
                                              ))
                                          .toList(),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Text(
                              'Treatments',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            SizedBox(
                              width: 30,
                            ),
                            InkWell(
                                onTap: () => openTreatmentDialog(context),
                                child: Text(
                                  '<- Click To Select',
                                  style: TextStyle(
                                    color: Theme.of(context).accentColor,
                                    fontWeight: FontWeight.w700,
                                  ),
                                )),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              width: 300,
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(2)),
                              child: selectedTreatments.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('No data to display'),
                                    )
                                  : ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: selectedTreatments.length,
                                      itemBuilder: (context, count) {
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                              selectedTreatments[count] == null
                                                  ? 'Not Set'
                                                  : selectedTreatments[count]),
                                        );
                                      },
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: DropdownWidget(
                      accentColor: EmpresColors.textFieldColor,
                      primaryColor: EmpresColors.textFieldColor,
                      width: 300,
                      label: Text('Surveillance Type'),
                      hint: Text('Surveillance Type'),
                      items: surveillanceTypeList,
                      onChanged: (value) {},
                      boxTextstyle: TextStyle(
                          fontFamily: 'IBMPlexSans',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: EmpresColors.labelTextColor),
                      style: TextStyle(
                          fontFamily: 'IBMPlexSans',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: EmpresColors.labelTextColor),
                      accentTextstyle: TextStyle(
                          fontFamily: 'IBMPlexSans',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Flexible(
                          fit: FlexFit.loose,
                          child: Text(
                            'Infection Sources',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 30,
                        ),
                        Flexible(
                          fit: FlexFit.loose,
                          child: InkWell(
                              onTap: () => openInfectionSourceDialog(context),
                              child: Text(
                                '<- Click To Select',
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.w700,
                                ),
                              )),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: 300,
                          padding: const EdgeInsets.all(10.0),
                          decoration: BoxDecoration(
                              border: Border.all(color: Colors.grey),
                              borderRadius: BorderRadius.circular(2)),
                          child: selectedInfectionSources.length == 0
                              ? Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text('No data to display'),
                                )
                              : ListView.builder(
                                  shrinkWrap: true,
                                  itemCount: selectedInfectionSources.length,
                                  itemBuilder: (context, count) {
                                    return Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text(selectedInfectionSources[
                                                  count] ==
                                              null
                                          ? 'Not Set'
                                          : selectedInfectionSources[count]),
                                    );
                                  },
                                ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: SizedBox(
                      width: 300,
                      child: BootstrapStyledTextfield(
                        controller: epidemiologyCommentController,
                        containerWidth: 300,
                        hintText: 'Epidemiological Comments',
                        paddingRight: 8.0,
                        paddingTop: 8.0,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save, color: Colors.white),
                          text: Text('Save Progress',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Save Progress',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.play_arrow, color: Colors.white),
                          text: Text('Next',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Next',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void openTreatmentDialog(BuildContext context) {
    showDialog(
        context: context,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height / 2,
            width: 400,
            child: Dialog(
              elevation: 30.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)),
              child: Column(
                children: <Widget>[
                  Container(
                    padding: EdgeInsets.all(12),
                    alignment: Alignment.centerLeft,
                    decoration: BoxDecoration(
                      color: Colors.grey[300],
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(15),
                          topRight: const Radius.circular(15)),
                    ),
                    child: Text('Treatments'),
                  ),
                  SizedBox(
                    height: 20.0,
                  ),
                  Flexible(
                    child: Container(
                      child: ListView(
                        children: treatmentList
                            .map((treatment) => TreatmentWidget(
                                treatment: treatment,
                                onCheckboxItemChecked: (value) {
                                  setState(() {
                                    var item = treatment;
                                    if (value == true) {
                                      if (selectedTreatments.contains(item) &&
                                          selectedTreatments != null) {
                                        selectedTreatments.remove(item);
                                        print('Removed treatment $item');
                                      } else {
                                        selectedTreatments.add(item);
                                        print('Added treatment $item');
                                      }
                                    } else {
                                      selectedTreatments.remove(item);
                                      print('Removed treatment $item');
                                    }
                                  });
                                  print('Selected treatment:$value');
                                }))
                            .toList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: Text('Save'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void openControlMeasuresDialog(BuildContext context) {
    showDialog(
        context: context,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height / 2,
            width: 400,
            child: Dialog(
              elevation: 30.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(12),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15)),
                      ),
                      child: Text('Control Measures')),
                  SizedBox(
                    height: 20.0,
                  ),
                  Flexible(
                    child: Container(
                      child: ListView(
                        children: controlMeasureList
                            .map(
                              (measure) => ControlMeasureWidget(
                                  measure: measure,
                                  onCheckboxItemChecked: (value) {
                                    setState(() {
                                      var item = measure;
                                      if (value == true) {
                                        if (selectedControlMeasures
                                                .contains(item) &&
                                            selectedControlMeasures != null) {
                                          selectedControlMeasures.remove(item);
                                          print('Removed measure $item');
                                        } else {
                                          selectedControlMeasures.add(item);
                                          print('Added measure $item');
                                        }
                                      } else {
                                        selectedControlMeasures.remove(item);
                                        print('Removed measure $item');
                                      }
                                    });
                                    print('Selected measure:$measure');
                                  }),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: Text('Save'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  void openInfectionSourceDialog(BuildContext context) {
    showDialog(
        context: context,
        child: Center(
          child: Container(
            color: Colors.transparent,
            height: MediaQuery.of(context).size.height / 2,
            width: 400,
            child: Dialog(
              elevation: 30.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15)),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(12),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15)),
                      ),
                      child: Text(
                        'Infection Sources',
                        style: TextStyle(
                            color: Colors.black87, fontWeight: FontWeight.bold),
                      )),
                  SizedBox(
                    height: 20.0,
                  ),
                  Flexible(
                    child: Container(
                      child: ListView(
                        children: infectionSourceList
                            .map((source) => InfectionSourceWidget(
                                source: source,
                                onCheckboxItemChecked: (value) {
                                  setState(() {
                                    var item = source;
                                    if (value == true) {
                                      if (selectedInfectionSources
                                              .contains(item) &&
                                          selectedInfectionSources != null) {
                                        selectedInfectionSources.remove(item);
                                        print('Removed source $item');
                                      } else {
                                        selectedInfectionSources.add(item);
                                        print('Added source $item');
                                      }
                                    } else {
                                      selectedInfectionSources.remove(item);
                                      print('Removed source $item');
                                    }
                                  });
                                  print('Selected '
                                      'source:$source');
                                }))
                            .toList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: Text('Save'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ));
  }

  void showToast(String message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }
}

class ControlMeasureWidget extends StatefulWidget {
  ControlMeasureWidget(
      {Key key, @required this.measure, @required this.onCheckboxItemChecked})
      : super(key: key);

  final String measure;
  final ValueChanged<bool> onCheckboxItemChecked;

  @override
  State<StatefulWidget> createState() {
    return ControlMeasureWidgetState();
  }
}

class ControlMeasureWidgetState extends State<ControlMeasureWidget> {
  bool defaultValue = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Checkbox(
              value: defaultValue,
              activeColor: Theme.of(context).accentColor,
              onChanged: (bool value) {
                setState(() {
                  defaultValue = value;
                  widget.onCheckboxItemChecked(value);
                });
              }),
          SizedBox(
            width: 5.0,
          ),
          Expanded(
            child: Text(widget.measure),
          ),
        ],
      ),
    );
  }
}

class TreatmentWidget extends StatefulWidget {
  TreatmentWidget(
      {Key key, @required this.treatment, @required this.onCheckboxItemChecked})
      : super(key: key);

  final String treatment;
  final ValueChanged<bool> onCheckboxItemChecked;

  @override
  State<StatefulWidget> createState() {
    return TreatmentWidgetState();
  }
}

class TreatmentWidgetState extends State<TreatmentWidget> {
  bool defaultValue = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Checkbox(
              value: defaultValue,
              activeColor: Theme.of(context).accentColor,
              onChanged: (bool value) {
                setState(() {
                  defaultValue = value;
                  widget.onCheckboxItemChecked(value);
                });
              }),
          SizedBox(
            width: 5.0,
          ),
          Expanded(child: Text(widget.treatment)),
        ],
      ),
    );
  }
}

class InfectionSourceWidget extends StatefulWidget {
  InfectionSourceWidget(
      {Key key, @required this.source, @required this.onCheckboxItemChecked})
      : super(key: key);

  final String source;
  final ValueChanged<bool> onCheckboxItemChecked;

  @override
  State<StatefulWidget> createState() {
    return InfectionSourceWidgetState();
  }
}

class InfectionSourceWidgetState extends State<InfectionSourceWidget> {
  bool defaultValue = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Checkbox(
              value: defaultValue,
              activeColor: Theme.of(context).accentColor,
              onChanged: (bool value) {
                setState(() {
                  defaultValue = value;
                  widget.onCheckboxItemChecked(value);
                });
              }),
          SizedBox(
            width: 5.0,
          ),
          Expanded(child: Text(widget.source)),
        ],
      ),
    );
  }
}
