import 'package:flutter/material.dart';

import 'dropdown_below.dart';

class DropdownWidget<T> extends StatefulWidget {
  DropdownWidget(
      {Key key,
      this.label,
      this.accentColor,
      this.items,
      this.value,
      this.primaryColor,
      this.width,
      this.hint,
      this.onChanged,
      this.boxTextstyle,
      this.itemTextstyle,
      this.style,
      this.accentTextstyle})
      : super(key: key);

  final Color primaryColor;
  final Color accentColor;
  final Widget label;
  final double width;
  final List<DropdownMenuItem<T>> items;
  final T value;
  final Widget hint;
  final ValueChanged<T> onChanged;
  final TextStyle style;
  final TextStyle boxTextstyle;
  final TextStyle itemTextstyle;
  final TextStyle accentTextstyle;

  @override
  _DropdownWidgetState createState() => _DropdownWidgetState();
}

class _DropdownWidgetState extends State<DropdownWidget> {
  Color color;
  Color borderColor;
  Color containerColor;

  @override
  initState() {
    super.initState();
    color = widget.primaryColor;
    borderColor = widget.primaryColor;
    containerColor = Colors.transparent;
  }

  Widget build(BuildContext context) {
    return InkWell(
        hoverColor: Colors.transparent,
        splashColor: Colors.transparent,
        highlightColor: Colors.transparent,
        focusColor: Colors.transparent,
        onTap: () {
          //You can leave it empty, like that.
          print('menu tapped');
        },
        onHover: (isHovering) {
          if (isHovering) {
            setState(() {
              color = widget.accentColor;
              borderColor = widget.accentColor;
            });
          } else {
            setState(() {
              color = widget.primaryColor;
              borderColor = widget.primaryColor;
            });
          }
        },
        child: Container(
          width: widget.width,
          //padding: EdgeInsets.only(top: 10, right: 10),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(5),
            child: Container(
              //height: 30,
              width: 200,
              decoration: BoxDecoration(
                border: Border.all(color: borderColor),
                borderRadius: BorderRadius.circular(5),
                boxShadow: [
                  BoxShadow(
                    blurRadius: 10,
                    spreadRadius: 1,
                    offset: Offset(2, 6),
                    color: Color.fromARGB(
                      5,
                      0,
                      0,
                      0,
                    ),
                  ),
                ],
                color: Color.fromARGB(255, 238, 238, 238),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Expanded(
                    flex: 1,
                    child: Container(
                        height: 40,
                        color: color,
                        padding: EdgeInsets.only(left: 10, top: 7),
                        child: widget.label),
                  ),
                  Expanded(
                      flex: 1,
                      child: Container(
                        padding: EdgeInsets.only(left: 10),
                        child: DropdownBelow(
                          itemWidth: widget.width / 2,
                          isDense: true,
                          elevation: 0,
                          color: widget.primaryColor,

                          itemTextstyle: widget.itemTextstyle,
                          boxTextstyle: widget.boxTextstyle,
                          style: widget.style,
                          //boxPadding: EdgeInsets.fromLTRB(13, 12, 0, 12),
                          boxHeight: 25,

                          hint: widget.hint,
                          value: widget.value,
                          items: widget.items,
                          onChanged: widget.onChanged,
                          accentTextstyle: widget.accentTextstyle,
                          accentColor: widget.accentColor,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ));
  }
}
