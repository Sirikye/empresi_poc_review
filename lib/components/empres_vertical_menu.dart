import 'package:flutter/material.dart';

class EmpresVerticalMenu extends StatefulWidget {
  final Widget child;

  const EmpresVerticalMenu({Key key, this.child}) : super(key: key);
  @override
  _EmpresVerticalMenuState createState() => _EmpresVerticalMenuState();
}

class _EmpresVerticalMenuState extends State<EmpresVerticalMenu> {
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.start,
      children: [
        Container(
          width: 200,
          height: 100,
          color: Colors.grey,
        ),
        Expanded(child: widget.child)
      ],
    );
  }
}
