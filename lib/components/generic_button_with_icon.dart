import 'package:flutter/material.dart';

class GenericButtonWithIcon extends StatefulWidget {
  final Widget icon;
  final Widget text;
  final Color color;
  final Color accentColor;
  final EdgeInsets textPadding;
  final EdgeInsets iconPadding;
  final Widget accentText;
  final Widget accentIcon;
  final Function() onTap;

  GenericButtonWithIcon(
      {Key key,
      this.icon,
      this.text,
      this.color,
      this.accentColor,
      @required this.textPadding,
      @required this.iconPadding,
      this.onTap,
      this.accentText,
      this.accentIcon})
      : super(key: key);

  @override
  _GenericButtonWithIconState createState() => _GenericButtonWithIconState();
}

class _GenericButtonWithIconState extends State<GenericButtonWithIcon> {
  Widget text;
  Widget icon;
  Color cardColor;

  @override
  void initState() {
    super.initState();
    cardColor = widget.color;
    text = widget.text;
    icon = widget.icon;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      hoverColor: Colors.transparent,
      splashColor: Colors.transparent,
      highlightColor: Colors.transparent,
      onTap: widget.onTap,
      onHover: (hovering) {
        if (hovering) {
          setState(() {
            text = widget.accentText;
            icon = widget.accentIcon;
          });
        } else {
          setState(() {
            text = widget.text;
            icon = widget.icon;
          });
        }
      },
      child: Card(
        color: cardColor,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: widget.textPadding,
              child: text,
            ),
            Padding(
              padding: widget.iconPadding,
              child: icon,
            ),
          ],
        ),
      ),
    );
  }
}
