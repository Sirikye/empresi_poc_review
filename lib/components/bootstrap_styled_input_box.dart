import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BootstrapStyledInputBox extends StatefulWidget {
  final String labelText;
  final String hintText;
  final int flexLeft;
  final int flexRight;
  final Widget child;

  BootstrapStyledInputBox(
      {Key key,
      @required this.labelText = "Label text",
      @required this.hintText = "Put the hint here",
      @required this.flexLeft = 8,
      @required this.flexRight = 9,
      @required this.child})
      : super(key: key);

  @override
  _BootstrapStyledInputBoxState createState() =>
      _BootstrapStyledInputBoxState();
}

class _BootstrapStyledInputBoxState extends State<BootstrapStyledInputBox> {
  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Container(
        height: 40,
        width: 200,
        decoration: BoxDecoration(
          border: Border.all(color: EmpresColors.textFieldColor),
          borderRadius: BorderRadius.circular(5),
          boxShadow: [
            BoxShadow(
              blurRadius: 10,
              spreadRadius: 1,
              offset: Offset(2, 6),
              color: Color.fromARGB(
                5,
                0,
                0,
                0,
              ),
            ),
          ],
          color: Color.fromARGB(255, 255, 255, 255),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Expanded(
              flex: widget.flexLeft,
              child: Container(
                  height: 40,
                  color: EmpresColors.textFieldColor,
                  padding: EdgeInsets.only(left: 10, top: 7),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Flexible(
                        child: Text(
                          widget.labelText,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor,
                              fontSize: 12,
                              fontFamily: 'IBMPlexSans'),
                        ),
                      ),
                      SizedBox(
                        height: 2.0,
                      ),
                      Flexible(
                        child: Text(
                          widget.labelText,
                          style: TextStyle(
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor,
                              fontSize: 10,
                              fontFamily: 'IBMPlexSans'),
                        ),
                      ),
                    ],
                  )),
            ),
            Expanded(flex: widget.flexRight, child: widget.child),
          ],
        ),
      ),
    );
  }
}
