import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SaveProgressButton extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _SaveProgressButtonState();
  }
}

class _SaveProgressButtonState extends State<SaveProgressButton> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      highlightColor: Colors.teal,
      hoverColor: Colors.orange,
      splashColor: Colors.red,
      onTap: () {
        print('Button clicked');
      },
      child: Card(
        color: Colors.blue,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
                padding: const EdgeInsets.all(5.0),
                child: Text('Save Progress')),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: Icon(
                Icons.save,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
