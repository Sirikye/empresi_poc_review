import 'package:empresi_poc/components/bootstrap_styled_date_picker.dart';
import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GeneticInformationWidget extends StatefulWidget {
  @override
  _GeneticInformationWidgetState createState() =>
      _GeneticInformationWidgetState();
}

class _GeneticInformationWidgetState extends State<GeneticInformationWidget> {
  GlobalKey<FormState> _geneticInfoKey = GlobalKey<FormState>();
  var geneticInfoColumns = [
    'SIB Link',
    'GenBank',
    'Id',
    'Name',
    'Type',
    'H Subtype',
    'N Subtype',
    'Clade',
    'Host',
    'Collection Date',
    'Status of Genetic Link',
  ];
  var geneticInfoRowOne = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];
  var geneticInfoRowTwo = [
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
    '',
  ];

  TextEditingController _startDateController = TextEditingController();
  TextEditingController _endDateController = TextEditingController();
  TextEditingController _iDIsolateController = TextEditingController();
  TextEditingController _outbreakIDController = TextEditingController();
  DateTime pickedDate;

  String _startDate;
  String _endDate;

  List<DropdownMenuItem> _diseaseList = [
    DropdownMenuItem(
      child: Text('Anthrax'),
    ),
    DropdownMenuItem(
      child: Text('Brucellosis'),
    ),
  ];

  List<DropdownMenuItem> countryList = [
    DropdownMenuItem(
      child: Text('Albania'),
    ),
    DropdownMenuItem(
      child: Text('Egypt'),
    ),
  ];

  List<DropdownMenuItem> outPutModeList = [
    DropdownMenuItem(
      child: Text('Isolates vs Outbreaks'),
    ),
    DropdownMenuItem(
      child: Text('Outbreaks vs Isolates'),
    ),
  ];

  List<DropdownMenuItem> serotypeList = [
    DropdownMenuItem(
      child: Text('H1'),
    ),
    DropdownMenuItem(
      child: Text('H2'),
    ),
  ];

  Future _selectStartDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _startDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      print(_startDate);
      updateStartDate();
    });
  }

  void updateStartDate() {
    _startDateController.text =
        _startDate == null ? DateTime.now() : _startDate;
  }

  Future _selectEndDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _endDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      print(_endDate);
      updateEndDate();
    });
  }

  void updateEndDate() {
    _endDateController.text = _endDate == null ? DateTime.now() : _endDate;
  }

  @override
  Widget build(BuildContext context) {
    var selectedStatus = [];
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
          child: Builder(
            builder: (context) => Form(
              key: _geneticInfoKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Genetic Module Information',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledDatePicker(
                              labelText: 'Start Date',
                              hintText: 'dd/mm/yyy',
                              flexLeft: 1,
                              flexRight: 1,
                              child: TextFormField(
                                readOnly: false,
                                controller: _startDateController,
                                decoration:
                                    InputDecoration(border: InputBorder.none),
                              ),
                              icon: InkWell(
                                child: Icon(
                                  Icons.date_range,
                                  color: Color(0xFF666666),
                                ),
                                onTap: () {
                                  return _selectStartDate();
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledDatePicker(
                              labelText: 'End Date',
                              hintText: 'dd/mm/yyy',
                              flexLeft: 1,
                              flexRight: 1,
                              child: TextFormField(
                                readOnly: false,
                                controller: _endDateController,
                                decoration:
                                    InputDecoration(border: InputBorder.none),
                              ),
                              icon: InkWell(
                                child: Icon(
                                  Icons.date_range,
                                  color: Color(0xFF666666),
                                ),
                                onTap: () {
                                  return _selectEndDate();
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Disease'),
                          hint: Text('Disease'),
                          items: _diseaseList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Country'),
                          hint: Text('Country'),
                          items: countryList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Output Mode'),
                          hint: Text('Output Mode'),
                          items: outPutModeList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Serotype'),
                          hint: Text('Serotype'),
                          items: serotypeList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(
                        width: 300,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Text(
                                'Link Status',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                width: 20,
                              ),
                              InkWell(
                                  onTap: () =>
                                      openControlMeasuresDialog(context),
                                  child: Text(
                                    '<- Click To Select',
                                    style: TextStyle(
                                      color: Theme.of(context).accentColor,
                                      fontWeight: FontWeight.w700,
                                    ),
                                  )),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            SizedBox(
                              width: 300,
                              child: Container(
                                padding: const EdgeInsets.all(10.0),
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.grey),
                                    borderRadius: BorderRadius.circular(2)),
                                child: selectedStatus.length == 0
                                    ? Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text('No data to display'),
                                      )
                                    : ListView(
                                        shrinkWrap: true,
                                        children: selectedStatus
                                            .map((status) => Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Text(status),
                                                ))
                                            .toList(),
                                      ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledInputBox(
                              labelText: 'ID Outbreak',
                              flexRight: 1,
                              flexLeft: 1,
                              hintText: 'ID Outbreak',
                              child: TextFormField(
                                controller: _outbreakIDController,
                                readOnly: true,
                                keyboardType: TextInputType.text,
                                decoration:
                                    InputDecoration(border: InputBorder.none),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledInputBox(
                              labelText: 'ID Isolate',
                              flexRight: 1,
                              flexLeft: 1,
                              hintText: 'ID Isolate',
                              child: TextFormField(
                                controller: _iDIsolateController,
                                readOnly: true,
                                keyboardType: TextInputType.text,
                                decoration:
                                    InputDecoration(border: InputBorder.none),
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.search, color: Colors.white),
                          text: Text('Find Matches',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.search, color: Colors.black),
                          accentText: Text('Find Matches',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.list, color: Colors.white),
                          text: Text('Export List',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.list, color: Colors.black),
                          accentText: Text('Export List',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.cancel, color: Colors.white),
                          text: Text('Reset Options',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.cancel, color: Colors.black),
                          accentText: Text('Reset Options',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.done, color: Colors.white),
                          text: Text('Change Status',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.done, color: Colors.black),
                          accentText: Text('Change Status',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5.0)),
                    child: SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: SingleChildScrollView(
                        scrollDirection: Axis.vertical,
                        child: DataTable(
                            dividerThickness: 1.0,
                            columns: geneticInfoColumns.map((e) {
                              return DataColumn(
                                  label: Text(
                                e,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black),
                              ));
                            }).toList(),
                            rows: [
                              DataRow(
                                  cells: geneticInfoRowOne
                                      .map((e) => DataCell(Text(e)))
                                      .toList()),
                              DataRow(
                                  cells: geneticInfoRowTwo
                                      .map((e) => DataCell(Text(e)))
                                      .toList()),
                            ]),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void openControlMeasuresDialog(BuildContext context) {
    showDialog(
        context: context,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height / 2,
            width: 400,
            child: Dialog(
              elevation: 30.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(12),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15)),
                      ),
                      child: Text('Please Select')),
                  SizedBox(
                    height: 10.0,
                  ),
                  Flexible(
                    child: Container(
                        height: 400,
                        width: 300,
                        child: ListView(
                          children: [
                            'Absent',
                            'Proposed Link',
                            'Unknown',
                            'Validated Link'
                          ]
                              .map(
                                (status) => StatusWidget(
                                    status: status,
                                    onCheckboxItemChecked: (value) {
                                      setState(() {
                                        var item = status;
//                                        if (value == true) {
//                                          if (selectedControlMeasures
//                                                  .contains(item) &&
//                                              selectedControlMeasures != null) {
//                                            selectedControlMeasures
//                                                .remove(item);
//                                            print('Removed measure $item');
//                                          } else {
//                                            selectedControlMeasures.add(item);
//                                            print('Added measure $item');
//                                          }
//                                        } else {
//                                          selectedControlMeasures.remove(item);
//                                          print('Removed measure $item');
//                                        }
                                        print('Selected Status:$item');
                                      });
                                    }),
                              )
                              .toList(),
                        )),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: Text('Ok'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('Clear'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                        FlatButton(
                          child: Text('All'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }
}

class StatusWidget extends StatefulWidget {
  StatusWidget(
      {Key key, @required this.status, @required this.onCheckboxItemChecked})
      : super(key: key);

  final String status;
  final ValueChanged<bool> onCheckboxItemChecked;

  @override
  State<StatefulWidget> createState() {
    return StatusWidgetState();
  }
}

class StatusWidgetState extends State<StatusWidget> {
  bool defaultValue = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Checkbox(
              value: defaultValue,
              activeColor: Theme.of(context).accentColor,
              onChanged: (bool value) {
                setState(() {
                  defaultValue = value;
                  widget.onCheckboxItemChecked(value);
                });
              }),
          SizedBox(
            width: 5.0,
          ),
          Expanded(
            child: Text(widget.status),
          ),
        ],
      ),
    );
  }
}
