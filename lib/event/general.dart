import 'package:empresi_poc/components/bootstrap_styled_date_picker.dart';
import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/bootstrap_styled_textfield.dart';
import 'package:empresi_poc/components/empres_switch.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class GeneralInfo extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _GeneralInfoState();
  }
}

class _GeneralInfoState extends State<GeneralInfo> {
  GlobalKey<FormState> _generalInfoFormKey = GlobalKey<FormState>();
  bool _selectedSwitchValue = false;

  var _observationDateController = TextEditingController();
  var _reportingDateController = TextEditingController();
  var _creationDateController = TextEditingController();
  var _validationDateController = TextEditingController();
  var _validatingEpidemiologyController = TextEditingController();
  var _entryOfficerController = TextEditingController();
  var _publicUrlController = TextEditingController();
  var _generalCommentController = TextEditingController();

  String _confidentialValue = 'No';
  String _observationDate;
  String _reportingDate;
  String _creationDate;
  String _validationDate;

  DateTime pickedDate;

  void selectedConfidentialOption(bool value) {
    setState(() {
      _selectedSwitchValue = value;
      _selectedSwitchValue == true
          ? _confidentialValue = "Yes"
          : _confidentialValue = "No";
      debugPrint('Confidential Value is $_confidentialValue');
    });
  }

  /// Method to update observation date with new value
  void updateObservationDate() {
    _observationDateController.text =
        _observationDate == null ? DateTime.now() : _observationDate;
  }

  /// Method to update reporting date with new value
  void updateReportingDate() {
    _reportingDateController.text =
        _reportingDate == null ? DateTime.now() : _reportingDate;
  }

  /// Method to update validation date with new value
  void updateValidationDate() {
    _validationDateController.text =
        _validationDate == null ? DateTime.now() : _validationDate;
  }

  Future _selectObservationDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _observationDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      print(_observationDate);
      updateObservationDate();
    });
  }

  Future _selectReportingDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _reportingDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      print(_reportingDate);
      updateReportingDate();
    });
  }

  Future _selectValidationDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _validationDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      print(_validationDate);
      updateValidationDate();
    });
  }

  @override
  void initState() {
    super.initState();
    _reportingDateController.text =
        DateFormat('dd/MM/yyyy').format(DateTime.now()).toString();
    _creationDateController.text =
        DateFormat('dd/MM/yyyy hh:mm').format(DateTime.now()).toString();

    _confidentialValue = "No";
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
          child: Builder(
            builder: (context) => Form(
              key: _generalInfoFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, top: 10.0),
                    child: Text(
                      'General Information',
                      style: Theme.of(context).textTheme.headline6,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),

                  EmpresSwitch(
                    switchTitle: Text('Confidential'),
                    onSwitchValueChanged: (value) {
                      selectedConfidentialOption(value);
//                      debugPrint(
//                          'Selected switch value is $_selectedSwitchValue');
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledDatePicker(
                              labelText: 'Creation Date',
                              hintText: 'Creation Date',
                              flexLeft: 1,
                              flexRight: 1,
                              child: TextFormField(
                                readOnly: true,
                                controller: _creationDateController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                              icon: Icon(
                                Icons.date_range,
                                color: Color(0xFF666666),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledDatePicker(
                              hintText: 'dd/mm/yyy',
                              labelText: 'Date of reporting',
                              flexRight: 1,
                              flexLeft: 1,
                              child: TextFormField(
                                controller: _reportingDateController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                              icon: InkWell(
                                child: Icon(
                                  Icons.date_range,
                                  color: Color(0xFF666666),
                                ),
                                onTap: () {
                                  return _selectReportingDate();
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledDatePicker(
                              hintText: 'dd/mm/yyy',
                              labelText: 'Date of observation',
                              flexLeft: 1,
                              flexRight: 1,
                              icon: InkWell(
                                child: Icon(
                                  Icons.date_range,
                                  color: Color(0xFF666666),
                                ),
                                onTap: () {
                                  return _selectObservationDate();
                                },
                              ),
                              child: TextFormField(
                                controller: _observationDateController,
                                readOnly: true,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledDatePicker(
                              labelText: 'Validation Date',
                              hintText: 'dd/mm/yyy',
                              flexLeft: 1,
                              flexRight: 1,
                              child: TextFormField(
                                controller: _validationDateController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                              icon: InkWell(
                                child: Icon(
                                  Icons.date_range,
                                  color: Color(0xFF666666),
                                ),
                                onTap: () {
                                  return _selectValidationDate();
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledInputBox(
                              labelText: 'Validating Epidemiology',
                              flexRight: 1,
                              flexLeft: 1,
                              hintText: 'Validating Epidemiology',
                              child: TextFormField(
                                controller: _validatingEpidemiologyController,
                                readOnly: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: Container(
                            child: BootstrapStyledInputBox(
                              labelText: 'Data Entry Officer',
                              hintText: 'John,Doe',
                              flexLeft: 1,
                              flexRight: 1,
                              child: TextFormField(
                                controller: _entryOfficerController,
                                readOnly: true,
                                keyboardType: TextInputType.text,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                ),
                                validator: (value) {
                                  if (value.isEmpty || value.length < 3) {
                                    return 'Name is missing, make sure '
                                        'you are '
                                        'logged in';
                                  }
                                  return null;
                                },
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: SizedBox(
                      width: 300,
                      child: BootstrapStyledTextfield(
                        controller: _publicUrlController,
                        containerWidth: 300,
                        hintText: 'Public URL',
                        paddingRight: 8.0,
                        paddingTop: 8.0,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),

                  /// Widget for Comment

                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: SizedBox(
                      width: 300,
                      child: BootstrapStyledTextfield(
                        controller: _generalCommentController,
                        containerWidth: 300,
                        hintText: 'Comment',
                        paddingRight: 8.0,
                        paddingTop: 8.0,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),

                  /// Widget for save progress
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save, color: Colors.white),
                          text: Text('Save Progress',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save, color: Colors.black),
                          accentText: Text('Save Progress',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.play_arrow, color: Colors.white),
                          text: Text('Next',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon:
                              Icon(Icons.play_arrow, color: Colors.black),
                          accentText: Text('Next',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
