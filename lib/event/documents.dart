import 'dart:convert';
import 'dart:typed_data';

import 'package:empresi_poc/components/bootstrap_styled_date_picker.dart';
import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/bootstrap_styled_textfield.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/empres_switch.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:path/path.dart';
import 'package:toast/toast.dart';
import 'package:universal_html/html.dart';
import 'package:url_launcher/url_launcher.dart';

class DocumentsWidget extends StatefulWidget {
  @override
  _DocumentsWidgetState createState() => _DocumentsWidgetState();
}

class _DocumentsWidgetState extends State<DocumentsWidget> {
  GlobalKey<FormState> _documentsKey = GlobalKey<FormState>();

  TextEditingController _documentTitleController = TextEditingController();
  TextEditingController _reportDateController = TextEditingController();
  TextEditingController _documentController = TextEditingController();
  TextEditingController _authorController = TextEditingController();
  TextEditingController _linkController = TextEditingController();
  TextEditingController _summaryController = TextEditingController();

  var documentsColumns = [
    'ID',
    'Outbreak ID',
    'Link',
    'File',
    'Report Date/Title',
    'Category',
    'Source',
    'Rep.Officer',
  ];

  var documentList = [
    '200',
    '202021',
    'https://pub'
        '.dev/packages/url_launcher',
    'PDF',
    '20/07/2020/Anthrax Outbreak',
    'Inter'
        'net',
    'FAO Officer',
    'Abdul Faye'
  ];

  bool _selectedSwitchValue = false;

  String _publicValue = 'No';

  String _reportDate;

  DateTime pickedDate;

  String selectedCategory;

  String selectedDocumentType;

  String selectedReportingOfficer;

  String selectedReportingSource;

  String selectedLanguage;

  List<int> _selectedFile;
  Uint8List _fileBytesData;
  String fileName;

  List<DropdownMenuItem> contentList = [
    DropdownMenuItem(child: Text('Confirmed')),
    DropdownMenuItem(child: Text('Denied')),
    DropdownMenuItem(
      child: Text('Suspicion'),
    )
  ];

  List<DropdownMenuItem> documentTypeList = [
    DropdownMenuItem(child: Text('Internet Site')),
    DropdownMenuItem(child: Text('Journal Article')),
    DropdownMenuItem(
      child: Text('Press Release'),
    )
  ];

  List<DropdownMenuItem> officerList = [
    DropdownMenuItem(child: Text('John')),
    DropdownMenuItem(child: Text('Edward')),
    DropdownMenuItem(
      child: Text('Jane'),
    )
  ];

  List<DropdownMenuItem> sourceList = [
    DropdownMenuItem(child: Text('FAO')),
    DropdownMenuItem(child: Text('FAO Officer')),
    DropdownMenuItem(
      child: Text('CDC'),
    )
  ];

  List<DropdownMenuItem> languageList = [
    DropdownMenuItem(child: Text('Arabic')),
    DropdownMenuItem(child: Text('Albanian')),
    DropdownMenuItem(
      child: Text('Chinese'),
    )
  ];

  void selectedPublicOption(bool value) {
    setState(() {
      _selectedSwitchValue = value;
      _selectedSwitchValue == true ? _publicValue = "Yes" : _publicValue = "No";
      debugPrint('Public Value is $_publicValue');
    });
  }

  Future _selectReportingDate(BuildContext context) async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _reportDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      print(_reportDate);
      updateReportDate();
    });
  }

  void updateReportDate() {
    _reportDateController.text =
        _reportDate == null ? DateTime.now() : _reportDate;
  }

  Future _selectFile() async {
    InputElement fileElement = FileUploadInputElement();

    fileElement.multiple = false;
    fileElement.draggable = true;
    fileElement.title = 'Select Document';
    fileElement.accept = '.pdf,.png,.jpg,.doc,.xls,.docx,.ppt';
    fileElement.click();

    fileElement.onChange.listen((event) {
      File documentFile = fileElement.files.first;

      fileName = basename(documentFile.name);
      print('File Name:$fileName');

      setState(() {
        if (fileName == null) {
          return CircularProgressIndicator();
        }
        _documentController.text = fileName;
      });

      final FileReader fileReader = FileReader();

      fileReader.readAsDataUrl(documentFile);

      fileReader.onLoadEnd.listen((event) async {
        getOnChangeResult(fileReader.result);
      }).onError((handleError) {
        print('Error:$handleError');
      });
    });
  }

  void getOnChangeResult(Object result) {
    setState(() {
      _fileBytesData =
          Base64Decoder().convert(result.toString().split(',').last);
      _selectedFile = _fileBytesData;
      print('File Bytes: \n$_selectedFile');
    });
  }

//  Future<String> uploadFileToStorage(List<int> asset, String name) async {
//    String filePath = 'Documents/$name';
//    Firebase.StorageReference storageReference =
//        Firebase.storage().ref(filePath);
//    Firebase.UploadTaskSnapshot uploadTaskSnapshot =
//        await storageReference.put(asset).future;
//    Uri uri = await uploadTaskSnapshot.ref.getDownloadURL();
//    String url = uri.toString();
//    print('Url: $url');
//    saveUploadedFilePathToFireStore(url);
//    return url;
//  }

//  Future<DocumentReference> saveUploadedFilePathToFireStore(String path) {
//    DocumentEntry document = DocumentEntry(
//        _publicValue,
//        _documentTitleController.text,
//        selectedCategory,
//        selectedDocumentType,
//        _reportDateController.text,
//        selectedReportingOfficer,
//        selectedReportingSource,
//        selectedLanguage,
//        _authorController.text,
//        _linkController.text,
//        _summaryController.text,
//        _documentController.text,
//        path);
//
//    final CollectionReference collectionReference =
//        Firestore.instance.collection('documents');
//
//    return collectionReference
//        .add(document.documentsEntryToJson())
//        .then((value) {
//      DocumentReference documentReference = value;
//      String documentId = documentReference.documentID;
//      collectionReference
//          .document(documentId)
//          .setData({'document_id': documentId}, merge: true);
//      print('Document ID:$documentId');
//    }).catchError((onError) {
//      print('Save Error:$onError');
//    });
//  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
          child: Builder(
            builder: (context) => Form(
              key: _documentsKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Documents',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  EmpresSwitch(
                    switchTitle: Text('Public'),
                    onSwitchValueChanged: (value) {
                      selectedPublicOption(value);
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: SizedBox(
                      width: 300,
                      child: BootstrapStyledInputBox(
                        flexLeft: 1,
                        flexRight: 1,
                        labelText: 'Title',
                        hintText: 'Title',
                        child: TextFormField(
                          controller: _documentTitleController,
                          keyboardType: TextInputType.text,
                          decoration: InputDecoration(border: InputBorder.none),
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Document title is missing';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Content'),
                          hint: Text('Content'),
                          items: contentList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Document Type'),
                          hint: Text('Document Type'),
                          items: documentTypeList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledDatePicker(
                            flexLeft: 1,
                            flexRight: 1,
                            labelText: 'Report Date',
                            hintText: 'Report Date',
                            icon: InkWell(
                              child: Icon(
                                Icons.date_range,
                                color: Color(0xFF666666),
                              ),
                              onTap: () => _selectReportingDate(context),
                            ),
                            child: TextFormField(
                              controller: _reportDateController,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                              validator: (value) {
                                if (value == null) {
                                  return 'Report date is missing';
                                }
                                return null;
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Reporting Officer'),
                          hint: Text('Reporting Officer'),
                          items: officerList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Reporting Source'),
                          hint: Text('Reporting Source'),
                          items: sourceList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Language'),
                          hint: Text('Language'),
                          items: languageList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexLeft: 1,
                            flexRight: 1,
                            labelText: 'Author/Institution',
                            hintText: 'Content Writer',
                            child: TextFormField(
                              controller: _authorController,
                              keyboardType: TextInputType.text,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexLeft: 1,
                            flexRight: 1,
                            labelText: 'Link',
                            hintText: 'URL',
                            child: TextFormField(
                              controller: _linkController,
                              keyboardType: TextInputType.text,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledTextfield(
                            containerWidth: 300,
                            controller: _summaryController,
                            paddingRight: 10.0,
                            paddingTop: 10.0,
                            hintText: 'Summary',
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledTextfield(
                            containerWidth: 300,
                            controller: _documentController,
                            paddingRight: 10.0,
                            paddingTop: 10.0,
                            hintText: 'Document',
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.attach_file, color: Colors.white),
                          text: Text('Select File',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon:
                              Icon(Icons.attach_file, color: Colors.black),
                          accentText: Text('Select File',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.delete, color: Colors.white),
                          text: Text('Delete File',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.delete, color: Colors.black),
                          accentText: Text('Delete File',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: GenericButtonWithIcon(
                      icon: Icon(Icons.save, color: Colors.white),
                      text: Text('Save Document',
                          style: TextStyle(color: Colors.white)),
                      color: Colors.blue,
                      accentColor: Colors.red,
                      accentIcon: Icon(Icons.save, color: Colors.black),
                      accentText: Text('Save Document',
                          style: TextStyle(color: Colors.black)),
                      textPadding: EdgeInsets.only(left: 5),
                      iconPadding: EdgeInsets.all(5),
                      onTap: () {
                        print('tapped');
                      },
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Expanded(
                          child: Container(
                              child: Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: DataTable(
                                dividerThickness: 1.0,
                                columns: documentsColumns.map((e) {
                                  return DataColumn(
                                      label: Text(
                                    e,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ));
                                }).toList(),
                                rows: [
                                  DataRow(
                                      cells: documentList
                                          .map((e) => DataCell(Text(e)))
                                          .toList())
                                ]),
                          ),
                        ),
                      ))),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showToast(BuildContext context, String message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  _launchDocumentLink(BuildContext context, String documentLink) async {
    if (documentLink != null && await canLaunch(documentLink)) {
      await launch(documentLink);
    } else {
      showToast(context, 'Unable to open link');
    }
  }
}
