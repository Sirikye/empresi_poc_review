import 'package:flutter/material.dart';

class EmpresTabItem extends StatefulWidget {
  final GestureTapCallback onTabItemTap;
  final String tabItemTitle;
  final Widget tabItemIcon;

  const EmpresTabItem(
      {Key key,
      @required this.onTabItemTap,
      @required this.tabItemIcon,
      @required this.tabItemTitle})
      : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return EmpresTabItemState();
  }
}

class EmpresTabItemState extends State<EmpresTabItem> {
  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: InkWell(
        splashColor: Colors.blue[100],
        borderRadius: BorderRadius.circular(5.0),
        hoverColor: Colors.blue[50],
        onTap: widget.onTabItemTap,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              widget.tabItemIcon,
              SizedBox(
                width: 10.0,
              ),
              Text(widget.tabItemTitle)
            ],
          ),
        ),
      ),
    );
  }
}
