import 'package:flutter/material.dart';

class EmpresSwitch extends StatefulWidget {
  final Widget switchTitle;
  final ValueChanged<bool> onSwitchValueChanged;

  const EmpresSwitch(
      {Key key,
      @required this.switchTitle,
      @required this.onSwitchValueChanged})
      : super(key: key);

  @override
  _EmpresSwitchState createState() => _EmpresSwitchState();
}

class _EmpresSwitchState extends State<EmpresSwitch> {
  var switchValue = false;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: widget.switchTitle,
        ),
        SizedBox(
          width: 5.0,
        ),
        Switch(
          value: switchValue,
          onChanged: (value) {
            setState(() {
              switchValue = value;
              widget.onSwitchValueChanged(switchValue);
              debugPrint('Selected switch value is $switchValue');
            });
          },
        ),
      ],
    );
  }
}
