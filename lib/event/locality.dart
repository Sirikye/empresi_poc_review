import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:location/location.dart';
import 'package:toast/toast.dart';

class LocalityInformationWidget extends StatefulWidget {
  @override
  _LocalityInformationWidgetState createState() =>
      _LocalityInformationWidgetState();
}

class _LocalityInformationWidgetState extends State<LocalityInformationWidget> {
  List<DropdownMenuItem> regionsList = [
    DropdownMenuItem(child: Text('Africa')),
    DropdownMenuItem(child: Text('Asia')),
    DropdownMenuItem(child: Text('Americas')),
    DropdownMenuItem(child: Text('Europe')),
  ];
  GlobalKey<FormState> _localityFormKey = GlobalKey<FormState>();
//  final EventDataRepository _repository = EventDataRepository();
  Locale locale;
  TextEditingController localityController = TextEditingController();
  TextEditingController latitudeController = TextEditingController();
  TextEditingController longitudeController = TextEditingController();

  var selectedAdmin1;
  int selectedAdmin1Id;
  var selectedAdmin2Id;
  String selectedAdmin2;
  String selectedLocalitySource;
  String selectedLocalityQuality;

  String longitude;
  String latitude;

//  List<AdministratorOne> administratorOneList;
//  List<AdministratorTwo> administratorTwoList;
//  AdministratorOne selectAdministratorOne;
//  AdministratorTwo selectedAdministratorTwo;
//
//  List<Country> countryList;
//  Country selectedCountry;
  int selectedCountryId;

//  List<Region> regionList;
//  Region selectedRegion;
//  String selectedRegion;
  String selectedRegionId;

//  List<SubRegion> subRegionList;
//  SubRegion selectedSubRegion;
  String selectedSubRegionId;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      color: EmpresColors.backgroundColor,
      child: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            bottom: 20.0,
          ),
          child: Builder(
            builder: (context) => Form(
              key: _localityFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Locality Information',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Region'),
                          hint: Text('Region'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Sub-Region'),
                          hint: Text('Sub-Region'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Country'),
                          hint: Text('Country'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            hintText: 'E.g Village',
                            labelText: 'Locality',
                            child: TextFormField(
                              controller: localityController,
                              keyboardType: TextInputType.text,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Admin 1'),
                          hint: Text('Admin 1'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Admin 2'),
                          hint: Text('Choose Admin 2'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Text(
                      'Centroids',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save_alt, color: Colors.white),
                          text: Text('Get Admin1 Centroid',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Get Admin1 Centroid',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save_alt, color: Colors.white),
                          text: Text('Get Admin2 Centroid',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Get Admin2 Centroid',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Latitude',
                            hintText: '0.30331',
                            child: TextFormField(
                              controller: latitudeController,
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexLeft: 1,
                            flexRight: 1,
                            labelText: 'Longitude',
                            hintText: '34.2939',
                            child: TextFormField(
                              controller: longitudeController,
                              keyboardType: TextInputType.numberWithOptions(
                                  decimal: true),
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Locality Source'),
                          hint: Text('Locality Source'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Locality Quality'),
                          hint: Text('Locality Quality'),
                          items: regionsList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Color.fromARGB(255, 93, 85, 85)),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save, color: Colors.white),
                          text: Text('Save Progress',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save, color: Colors.black),
                          accentText: Text('Save Progress',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.play_arrow, color: Colors.white),
                          text: Text('Next',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon:
                              Icon(Icons.play_arrow, color: Colors.black),
                          accentText: Text('Next',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void showToast(String message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  void getUserLocationUpdates() {
    var locationData = getUserLocationData();
    locationData == null
        ? CircularProgressIndicator()
        : locationData.then((location) {
            if (location != null) {
              setState(() {
                longitude = location.longitude.toStringAsFixed(5);
                latitude = location.latitude.toStringAsFixed(6);
//                String locality = location.
                longitudeController.text = longitude;
                latitudeController.text = latitude;
                if (longitude.isNotEmpty && latitude.isNotEmpty) {
                  showToast('Location successfully returned');
                } else {
                  showToast('Unable to find GPS position,'
                      'verify that GPS is turned on');
                }
              });
            } else {
              showToast('Unable to find GPS position, '
                  'verify that GPS is turned on');
              print('LocationData is null');
            }
          });
  }

  Future<LocationData> getUserLocationData() async {
    var location = Location();
    try {
      var locationData = await location.getLocation();
      return locationData;
    } on PlatformException catch (pe) {
      if (pe.code == 'PERMISSION_DENIED') {
        print('Permission denied');
      }
      return null;
    }
  }
}
