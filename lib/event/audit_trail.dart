import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';

class AuditTrailWidget extends StatefulWidget {
  @override
  _AuditTrailWidgetState createState() => _AuditTrailWidgetState();
}

class _AuditTrailWidgetState extends State<AuditTrailWidget> {
  var auditTrailColumns = [
    'Modify Date',
    'Last Modified by',
    'Email',
    'Changes',
  ];
  var auditTrailRow = [
    '20/03/2020',
    'John Doe',
    'John.Doe@example.com',
    '02',
  ];

  /// Widget for Genetic Information Table
  Widget auditTrailTable() {
    return Card(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
      child: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: DataTable(
              dividerThickness: 1.0,
              columns: auditTrailColumns.map((e) {
                return DataColumn(
                    label: Text(
                  e,
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black),
                ));
              }).toList(),
              rows: [
                DataRow(
                    cells:
                        auditTrailRow.map((e) => DataCell(Text(e))).toList()),
                DataRow(
                    cells:
                        auditTrailRow.map((e) => DataCell(Text(e))).toList()),
              ]),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 20.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  'Audit Trail',
                  style: Theme.of(context).textTheme.headline5,
                ),
              ),
              SizedBox(
                height: 10.0,
              ),
              Row(
                children: <Widget>[
                  Expanded(child: Container(child: auditTrailTable())),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
