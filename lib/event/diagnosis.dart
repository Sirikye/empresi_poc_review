import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/empres_switch.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';
import 'package:toast/toast.dart';

class DiagnosisInformationWidget extends StatefulWidget {
  @override
  _DiagnosisInformationWidgetState createState() =>
      _DiagnosisInformationWidgetState();
}

class _DiagnosisInformationWidgetState
    extends State<DiagnosisInformationWidget> {
  GlobalKey<FormState> _diagnosisFormKey = GlobalKey<FormState>();

  TextEditingController cladeController = TextEditingController();
  List<String> subTypeMacroTableColumns = ['Serotype', 'Clade'];
  bool isUnverifiable = false;
  bool hasClinicalSigns = false;
  bool hasLesions = false;
  bool isFromLaboratoryTest = false;

  String selectedStatus;
  String selectedSource;
  var selectedDiseaseId;
  String unVerifiableValue;
  String clinicalSigns;
  String lesions;
  String laboratoryTest;

  String selectedSeroType;

  List<String> splitSubtypeMacroList;

  List<DropdownMenuItem> statusList = [
    DropdownMenuItem(
      child: Text('Confirmed'),
    ),
    DropdownMenuItem(
      child: Text('Suspected'),
    ),
  ];
  List<DropdownMenuItem> sourceList = [
    DropdownMenuItem(
      child: Text('FAO Officer'),
    ),
    DropdownMenuItem(
      child: Text('DV0 1'),
    ),
  ];
  List<DropdownMenuItem> _diseaseList = [
    DropdownMenuItem(
      child: Text('Anthrax'),
    ),
    DropdownMenuItem(
      child: Text('Brucellosis'),
    ),
  ];
  List<DropdownMenuItem> _serotypeList = [
    DropdownMenuItem(
      child: Text('H1'),
    ),
    DropdownMenuItem(
      child: Text('H2'),
    ),
    DropdownMenuItem(
      child: Text('H3'),
    ),
  ];

  var selectedSubTypeMacroList = ['H1', 'H2', 'H3'];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(
            left: 20.0,
            right: 20.0,
            bottom: 20.0,
          ),
          child: Builder(builder: (context) {
            return Form(
              key: _diagnosisFormKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Diagnosis Information',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  EmpresSwitch(
                      switchTitle: Text('Unverifiable'),
                      onSwitchValueChanged: (value) {
                        setState(() {
                          isUnverifiable = value;
                          if (isUnverifiable == true) {
                            unVerifiableValue = 'Yes';
                          } else {
                            unVerifiableValue = 'No';
                          }
                        });
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: 8.0,
                    ),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Status'),
                          hint: Text('Diagnosis Status'),
                          items: statusList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Source'),
                          hint: Text('Diagnosis Source'),
                          items: sourceList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: DropdownWidget(
                      accentColor: EmpresColors.textFieldColor,
                      primaryColor: EmpresColors.textFieldColor,
                      width: 300,
                      label: Text('Disease'),
                      hint: Text('Disease'),
                      items: _diseaseList,
                      onChanged: (value) {},
                      boxTextstyle: TextStyle(
                          fontFamily: 'IBMPlexSans',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: EmpresColors.labelTextColor),
                      style: TextStyle(
                          fontFamily: 'IBMPlexSans',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: EmpresColors.labelTextColor),
                      accentTextstyle: TextStyle(
                          fontFamily: 'IBMPlexSans',
                          fontSize: 12,
                          fontWeight: FontWeight.normal,
                          color: Colors.white),
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Serotye'),
                          hint: Text('Serotype'),
                          items: _serotypeList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Clade',
                            hintText: 'Clade',
                            child: TextFormField(
                                controller: cladeController,
                                keyboardType: TextInputType.text,
                                decoration:
                                    InputDecoration(border: InputBorder.none)),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: GenericButtonWithIcon(
                      icon: Icon(Icons.save, color: Colors.white),
                      text: Text('Save Subtype',
                          style: TextStyle(color: Colors.white)),
                      color: Colors.blue,
                      accentColor: Colors.red,
                      accentIcon: Icon(Icons.save, color: Colors.black),
                      accentText: Text('Save Subtype',
                          style: TextStyle(color: Colors.black)),
                      textPadding: EdgeInsets.only(left: 5),
                      iconPadding: EdgeInsets.all(5),
                      onTap: () {
                        print('tapped');
                      },
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Subtypes Macro',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Card(
                        elevation: 2.0,
                        color: EmpresColors.textInputFillColor,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(2.0)),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: DataTable(
                                dividerThickness: 2.0,
                                columns: subTypeMacroTableColumns.map((e) {
                                  return DataColumn(
                                      label: Text(
                                    e,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ));
                                }).toList(),
                                rows: selectedSubTypeMacroList
                                    .map(
                                      (subType) => DataRow(
                                          onSelectChanged: (value) {},
                                          cells: <DataCell>[
                                            DataCell(Text(subType)),
                                            DataCell(Text(subType)),
                                          ]),
                                    )
                                    .toList()),
                          ),
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Basis for Diagnosis',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Row(
                          children: <Widget>[
                            Text('Has Clinical Signs'),
                            SizedBox(
                              width: 10.0,
                            ),
                            Switch(
                              value: hasClinicalSigns,
                              onChanged: (value) {
                                setState(() {});
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Has Lesion'),
                            SizedBox(
                              width: 10.0,
                            ),
                            Switch(
                              value: hasLesions,
                              onChanged: (value) {
                                setState(() {});
                              },
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Text('Laboratory Test'),
                            SizedBox(
                              width: 10.0,
                            ),
                            Switch(
                              value: isFromLaboratoryTest,
                              onChanged: (value) {
                                setState(() {});
                              },
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save, color: Colors.white),
                          text: Text('Save Progress',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Save Progress',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.play_arrow, color: Colors.white),
                          text: Text('Next',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save_alt, color: Colors.black),
                          accentText: Text('Next',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  )
                ],
              ),
            );
          }),
        ),
      ),
    );
  }

  void showToast(String message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }
}
