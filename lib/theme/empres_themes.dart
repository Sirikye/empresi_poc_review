import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';

class EmpresTheme {
  /// Empres app main theme
  static ThemeData empresTheme = ThemeData(
    primaryColor: EmpresColors.primaryColor,
    accentColor: EmpresColors.accentColor,
    primaryColorDark: EmpresColors.primaryColorDark,
//    inputDecorationTheme: empresInputDecorationTheme,
//    cursorColor: EmpresColors.inputCursorColor,
  );

  /// Input Decoration Theme
//  static InputDecorationTheme empresInputDecorationTheme = InputDecorationTheme(
//      contentPadding: EdgeInsets.symmetric(vertical: 5.0, horizontal: 5.0),
//      border: InputBorder.none,
//      filled: true,
//      hintStyle: empresHintStyle,
//      labelStyle: empresLabelStyle,
//      fillColor: EmpresColors.textInputFillColor,
//      focusColor: EmpresColors.textInputFocusColor,
//      focusedBorder: UnderlineInputBorder(
//        borderSide: BorderSide(color: EmpresColors.focusedInputColor),
//      ),
//      enabledBorder: UnderlineInputBorder(
//        borderSide: BorderSide(color: EmpresColors.enabledInputColor),
//      ),
//      errorBorder: UnderlineInputBorder(
//        borderSide: BorderSide(color: EmpresColors.asteriskColor),
//      ));

  /// Input Hint Style
//  static TextStyle empresHintStyle = TextStyle(
//    color: EmpresColors.hintTextColor,
//    fontStyle: FontStyle.normal,
//    fontWeight: FontWeight.w500,
//    fontSize: 15,
//  );

  /// Input Label Style
//  static TextStyle empresLabelStyle = TextStyle(
//      color: EmpresColors.labelTextColor,
//      fontStyle: FontStyle.normal,
//      fontWeight: FontWeight.w500,
//      fontSize: 15);
}
