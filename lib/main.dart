import 'package:easy_localization/easy_localization.dart';
import 'package:empresi_poc/components/tab_item.dart';
import 'package:empresi_poc/event/audit_trail.dart';
import 'package:empresi_poc/event/documents.dart';
import 'package:empresi_poc/event/epidemiology.dart';
import 'package:empresi_poc/event/general.dart';
import 'package:empresi_poc/event/genetics_module.dart';
import 'package:empresi_poc/event/laboratory_test.dart';
import 'package:empresi_poc/event/locality.dart';
import 'package:flutter/material.dart';
import 'package:vertical_tabs/vertical_tabs.dart';

import 'event/control_measures.dart';
import 'event/diagnosis.dart';

void main() {
  runApp(EasyLocalization(
    child: MyApp(),
    supportedLocales: [
      Locale('en'),
      Locale('es'),
      Locale('fr'),
    ],
    fallbackLocale: Locale('en'),
    startLocale: Locale('en'),
    path: 'assets/translations',
  ));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      localizationsDelegates: context.localizationDelegates,
      supportedLocales: context.supportedLocales,
      locale: context.locale,
      title: 'Empresi Poc',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final GlobalKey _scaffoldKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        actions: [
          InkWell(
            hoverColor: Colors.transparent,
            onTap: () {},
            child: Row(
              children: [
                Text('Language'),
                SizedBox(
                  width: 5.0,
                ),
                Icon(
                  Icons.language,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          InkWell(
            hoverColor: Colors.transparent,
            onTap: () {},
            child: Row(
              children: [
                Text('Message'),
                SizedBox(
                  width: 5.0,
                ),
                Icon(
                  Icons.message,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          SizedBox(
            width: 20.0,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 30.0),
            child: InkWell(
              hoverColor: Colors.transparent,
              onTap: () {},
              child: Row(
                children: [
                  Text('Login'),
                  SizedBox(
                    width: 5.0,
                  ),
                  Icon(
                    Icons.exit_to_app,
                    color: Colors.white,
                  ),
                ],
              ),
            ),
          ),
        ],
        title: Row(
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 30.0),
              child: Image.asset(
                'assets/logo/fao_logo_white.png',
                fit: BoxFit.contain,
                height: 40.0,
              ),
            )
          ],
        ),
      ),
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Padding(
          padding: const EdgeInsets.only(
              left: 50.0, top: 10.0, right: 30.0, bottom: 10.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    InkWell(
                      hoverColor: Colors.transparent,
                      onTap: () {},
                      child: CircleAvatar(
                        backgroundColor: Colors.blue,
                        radius: 15.0,
                        child: Icon(
                          Icons.menu,
                          color: Colors.white,
                        ),
                      ),
                    ),
                    SizedBox(
                      width: 40,
                    ),
                    EmpresTabItem(
                      onTabItemTap: () {
                        print('Dashboard tapped');
                      },
                      tabItemTitle: 'title_dashboard'.tr(),
                      tabItemIcon: Icon(
                        Icons.dashboard,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    EmpresTabItem(
                      onTabItemTap: () {
                        print('Create Event tapped');
                      },
                      tabItemTitle: 'title_create_event'.tr(),
                      tabItemIcon: Icon(
                        Icons.location_on,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    EmpresTabItem(
                      onTabItemTap: () {
                        print('Manage Events tapped');
                      },
                      tabItemTitle: 'title_manage_events'.tr(),
                      tabItemIcon: Icon(
                        Icons.list,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      width: 30,
                    ),
                    EmpresTabItem(
                      onTabItemTap: () {
                        print('Analysis & Reporting tapped');
                      },
                      tabItemTitle: 'title_analysis_report'.tr(),
                      tabItemIcon: Icon(
                        Icons.trending_up,
                        color: Colors.grey,
                      ),
                    ),
                    SizedBox(
                      width: 100,
                    ),
                    Flexible(
                      child: Container(
                        width: 200,
                        height: 40,
                        child: TextField(
                          decoration: InputDecoration(
                              suffixIcon: IconButton(
                                  icon: Icon(
                                    Icons.search,
                                    color: Colors.blue,
                                  ),
                                  onPressed: () {}),
                              hintText: 'SEARCH',
                              border: OutlineInputBorder()),
                        ),
                      ),
                    )
                  ],
                ),
              ),
              Expanded(
                child: Row(
                  children: [
                    Expanded(
                      flex: 3,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: VerticalTabs(
                          indicatorSide: IndicatorSide.end,
                          indicatorColor: Colors.blue,
                          tabsWidth: 200,
                          tabs: <Tab>[
                            Tab(
                              child: Padding(
                                padding: const EdgeInsets.all(10.0),
                                child: Row(
                                  children: [
                                    SizedBox.fromSize(
                                      size: Size(18.0, 18.0),
                                      child: Image.asset(
                                        'assets/menu/general_info_black.png',
                                        fit: BoxFit.contain,
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0,
                                    ),
                                    Text('General Info'),
                                  ],
                                ),
                              ),
                            ),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/general_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Locality Info'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/diagnosis_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Diagnosis'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/general_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Epidemiology'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/diagnosis_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Control Measures'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/general_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Laboratory'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/general_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Documents'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/general_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Genetics'),
                                ],
                              ),
                            )),
                            Tab(
                                child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Row(
                                children: [
                                  SizedBox.fromSize(
                                    size: Size(18.0, 18.0),
                                    child: Image.asset(
                                      'assets/menu/general_info_black.png',
                                      fit: BoxFit.contain,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Text('Audit Trail'),
                                ],
                              ),
                            )),
                          ],
                          contents: <Widget>[
                            GeneralInfo(),
                            LocalityInformationWidget(),
                            DiagnosisInformationWidget(),
                            EpidemiologyInformationWidget(),
                            ControlMeasuresWidget(),
                            LaboratoryTestInformationWidget(),
                            DocumentsWidget(),
                            GeneticInformationWidget(),
                            AuditTrailWidget()
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 30.0),
                        child: Container(
                          color: Color(0xFF666666),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Center(
                                  child: Text('Overview Content will be here'),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
