import 'package:empresi_poc/components/bootstrap_styled_date_picker.dart';
import 'package:empresi_poc/components/bootstrap_styled_input_box.dart';
import 'package:empresi_poc/components/dropdown_widget.dart';
import 'package:empresi_poc/components/empres_switch.dart';
import 'package:empresi_poc/components/generic_button_with_icon.dart';
import 'package:empresi_poc/theme/empres_colors.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:toast/toast.dart';

class LaboratoryTestInformationWidget extends StatefulWidget {
  @override
  _LaboratoryTestInformationWidgetState createState() =>
      _LaboratoryTestInformationWidgetState();
}

class _LaboratoryTestInformationWidgetState
    extends State<LaboratoryTestInformationWidget> {
  GlobalKey<FormState> _laboratoryTestInformationKey = GlobalKey<FormState>();
  var accentColor = EmpresColors.accentColor;
  String selectedResult;

  String selectedDisease;
  String selectedTest;

  var _selectedSwitchValue = false;
  String _humanAffectedValue;

  var _otherLaboratoryController = TextEditingController();
  var _samplingDateController = TextEditingController();
  var _resultsDateController = TextEditingController();

  var pickedDate;
  String _resultsDate;
  String _samplingDate;

  var labDataColumns = [
    'Disease Tested',
    'Species Tested',
    'Human Tested',
    'Samples',
    'Test',
    'Results',
    'Date of Results',
    'Laboratory',
    'Other Laboratory',
  ];
  var labRowList = [
    'Anthrax',
    'Cattle',
    'No',
    'Blood sample',
    'Coaginn test',
    'Po'
        'sit'
        'ive',
    '20/07/2020',
    'NADDEC',
    'No'
  ];

  List<DropdownMenuItem> diseaseList = [
    DropdownMenuItem(child: Text('Anthrax'))
  ];
  List<DropdownMenuItem> speciesList = [
    DropdownMenuItem(child: Text('Cattle'))
  ];
//  List<DropdownMenuItem> diseaseList = [DropdownMenuItem(child: Text('Anthrax'))];

  Locale locale;

  List<String> selectedSamples = [];
  List<String> selectedSpecies = [];

  int selectedCountryId;

  String selectedLab;

  var sampleList = ['Blood Sample', 'Body Fluid'];

  void updateSamplingDate() {
    _samplingDateController.text =
        _samplingDate == null ? DateTime.now() : _samplingDate;
  }

  void updateResultsDate() {
    _resultsDateController.text =
        _resultsDate == null ? DateTime.now() : _resultsDate;
  }

  Future _selectResultsDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _resultsDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      debugPrint("Results date:$_resultsDate");
      updateResultsDate();
    });
  }

  Future _selectSamplingDate() async {
    pickedDate = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(2016),
        lastDate: DateTime(2021));
    setState(() {
      _samplingDate = DateFormat('dd/MM/yyyy').format(pickedDate).toString();
      debugPrint("Sampling date:$_samplingDate");
      updateSamplingDate();
    });
  }

  void humanAffectedOption(bool value) {
    setState(() {
      _selectedSwitchValue = value;
      if (_selectedSwitchValue == true) {
        _humanAffectedValue = "Yes";
      } else {
        _humanAffectedValue = "No";
      }
      debugPrint('Human Affected Value is $_humanAffectedValue');
    });
  }

  void showToast(String message) {
    Toast.show(message, context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  // ignore: must_call_super
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        height: MediaQuery.of(context).size.height,
        color: EmpresColors.backgroundColor,
        child: Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20, bottom: 20.0),
          child: Builder(
            builder: (context) => Form(
              key: _laboratoryTestInformationKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      'Laboratory Test Information',
                      style: Theme.of(context).textTheme.headline5,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  EmpresSwitch(
                      switchTitle: Text('Humans Affected'),
                      onSwitchValueChanged: (value) {
                        humanAffectedOption(value);
                      }),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Disease'),
                          hint: Text('Disease'),
                          items: diseaseList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Test'),
                          hint: Text('Test'),
                          items: diseaseList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Container(
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Flexible(
                                child: Text(
                                  'Samples',
                                  style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 10,
                              ),
                              Flexible(
                                child: InkWell(
                                    onTap: () => openSamplesDialog(context),
                                    child: Text(
                                      '<- Click To Select',
                                      style: TextStyle(
                                        color: Theme.of(context).accentColor,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    )),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(2)),
                              width: 300.0,
                              child: selectedSamples.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('No data to display'),
                                    )
                                  : ListView(
                                      shrinkWrap: true,
                                      children: selectedSamples
                                          .map((measure) => Padding(
                                                padding:
                                                    const EdgeInsets.all(8.0),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Text(measure),
                                                ),
                                              ))
                                          .toList(),
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: <Widget>[
                            Flexible(
                              child: Text(
                                'Species Selected',
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            SizedBox(
                              width: 10,
                            ),
                            Flexible(
                              child: InkWell(
                                  child: Text(
                                '<- Click To Select',
                                style: TextStyle(
                                  color: Theme.of(context).accentColor,
                                  fontWeight: FontWeight.w700,
                                ),
                              )),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Container(
                              padding: const EdgeInsets.all(10.0),
                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.grey),
                                  borderRadius: BorderRadius.circular(2)),
                              width: 300.0,
                              child: selectedSpecies.length == 0
                                  ? Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Text('No data to display'),
                                    )
                                  : ListView.builder(
                                      shrinkWrap: true,
                                      itemCount: selectedSpecies.length,
                                      itemBuilder: (context, count) {
                                        return Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                              selectedSpecies[count] == null
                                                  ? 'Not Set'
                                                  : selectedSpecies[count]),
                                        );
                                      },
                                    ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledDatePicker(
                            flexLeft: 1,
                            flexRight: 1,
                            labelText: 'Date of Sampling',
                            hintText: 'Date of Sampling',
                            icon: InkWell(
                              child: Icon(
                                Icons.date_range,
                                color: Color(0xFF666666),
                              ),
                              onTap: () {
                                return _selectSamplingDate();
                              },
                            ),
                            child: TextFormField(
                              controller: _samplingDateController,
                              readOnly: true,
                              decoration: InputDecoration(
                                border: InputBorder.none,
                              ),
                              onChanged: (value) {
                                setState(() {
                                  _samplingDate = value;
                                });
                              },
                            ),
                          ),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledDatePicker(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Date of Results',
                            hintText: 'Date of Results',
                            icon: InkWell(
                              child: Icon(
                                Icons.date_range,
                                color: Color(0xFF666666),
                              ),
                              onTap: () {
                                return _selectResultsDate();
                              },
                            ),
                            child: TextFormField(
                              controller: _resultsDateController,
                              readOnly: true,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                      padding: const EdgeInsets.only(left: 8.0),
                      child: Row(
                        children: [
                          DropdownWidget(
                            accentColor: EmpresColors.textFieldColor,
                            primaryColor: EmpresColors.textFieldColor,
                            width: 300,
                            label: Text('Result'),
                            hint: Text('Result'),
                            items: diseaseList,
                            onChanged: (value) {},
                            boxTextstyle: TextStyle(
                                fontFamily: 'IBMPlexSans',
                                fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: EmpresColors.labelTextColor),
                            style: TextStyle(
                                fontFamily: 'IBMPlexSans',
                                fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: EmpresColors.labelTextColor),
                            accentTextstyle: TextStyle(
                                fontFamily: 'IBMPlexSans',
                                fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: Colors.white),
                          ),
                          SizedBox(
                            width: 10.0,
                          ),
                          DropdownWidget(
                            accentColor: EmpresColors.textFieldColor,
                            primaryColor: EmpresColors.textFieldColor,
                            width: 300,
                            label: Text('Country'),
                            hint: Text('Country'),
                            items: diseaseList,
                            onChanged: (value) {},
                            boxTextstyle: TextStyle(
                                fontFamily: 'IBMPlexSans',
                                fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: EmpresColors.labelTextColor),
                            style: TextStyle(
                                fontFamily: 'IBMPlexSans',
                                fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: EmpresColors.labelTextColor),
                            accentTextstyle: TextStyle(
                                fontFamily: 'IBMPlexSans',
                                fontSize: 12,
                                fontWeight: FontWeight.normal,
                                color: Colors.white),
                          )
                        ],
                      )),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        DropdownWidget(
                          accentColor: EmpresColors.textFieldColor,
                          primaryColor: EmpresColors.textFieldColor,
                          width: 300,
                          label: Text('Laboratory'),
                          hint: Text('Laboratory'),
                          items: diseaseList,
                          onChanged: (value) {},
                          boxTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          style: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: EmpresColors.labelTextColor),
                          accentTextstyle: TextStyle(
                              fontFamily: 'IBMPlexSans',
                              fontSize: 12,
                              fontWeight: FontWeight.normal,
                              color: Colors.white),
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        SizedBox(
                          width: 300,
                          child: BootstrapStyledInputBox(
                            flexRight: 1,
                            flexLeft: 1,
                            labelText: 'Other Laboratory',
                            hintText: 'Other Laboratory',
                            child: TextFormField(
                              controller: _otherLaboratoryController,
                              keyboardType: TextInputType.text,
                              textCapitalization: TextCapitalization.sentences,
                              decoration:
                                  InputDecoration(border: InputBorder.none),
//          validator: (value) {
//            if (value.isEmpty) {
//              return 'Enter other laboratory';
//            }
//            return null;
//          },
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10.0,
                  ),
                  GenericButtonWithIcon(
                    icon: Icon(Icons.add_circle, color: Colors.white),
                    text:
                        Text('Add Test', style: TextStyle(color: Colors.white)),
                    color: Colors.blue,
                    accentColor: Colors.red,
                    accentIcon: Icon(Icons.add_circle, color: Colors.black),
                    accentText:
                        Text('Add Test', style: TextStyle(color: Colors.black)),
                    textPadding: EdgeInsets.only(left: 5),
                    iconPadding: EdgeInsets.all(5),
                    onTap: () {
                      print('tapped');
                    },
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          'Laboratory and Test List',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Card(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0)),
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: DataTable(
                                showCheckboxColumn: false,
                                dividerThickness: 1.0,
                                columns: labDataColumns.map((e) {
                                  return DataColumn(
                                      label: Text(
                                    e,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black),
                                  ));
                                }).toList(),
                                rows: [
                                  DataRow(
                                      cells: labRowList
                                          .map((e) => DataCell(Text(e)))
                                          .toList()),
                                ]),
                          ),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0),
                    child: Row(
                      children: [
                        GenericButtonWithIcon(
                          icon: Icon(Icons.save, color: Colors.white),
                          text: Text('Save Progress',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon: Icon(Icons.save, color: Colors.black),
                          accentText: Text('Save Progress',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        GenericButtonWithIcon(
                          icon: Icon(Icons.cloud_upload, color: Colors.white),
                          text: Text('Submit Event',
                              style: TextStyle(color: Colors.white)),
                          color: Colors.blue,
                          accentColor: Colors.red,
                          accentIcon:
                              Icon(Icons.cloud_upload, color: Colors.black),
                          accentText: Text('Submit Event',
                              style: TextStyle(color: Colors.black)),
                          textPadding: EdgeInsets.only(left: 5),
                          iconPadding: EdgeInsets.all(5),
                          onTap: () {
                            print('tapped');
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void openSamplesDialog(BuildContext context) {
    showDialog(
        context: context,
        child: Center(
          child: Container(
            height: MediaQuery.of(context).size.height / 2,
            width: 400,
            child: Dialog(
              elevation: 30.0,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0)),
              child: Column(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.all(12),
                      alignment: Alignment.centerLeft,
                      decoration: BoxDecoration(
                        color: Colors.grey[300],
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(15),
                            topRight: const Radius.circular(15)),
                      ),
                      child: Text('Samples')),
                  SizedBox(
                    height: 20.0,
                  ),
                  Flexible(
                    child: Container(
                      height: 400,
                      width: 300,
                      child: ListView(
                        children: sampleList
                            .map(
                              (sample) => SamplesWidget(
                                  sample: sample,
                                  onCheckboxItemChecked: (value) {
                                    setState(() {
                                      var item = sample;
                                      if (value == true) {
                                        if (selectedSamples.contains(item) &&
                                            selectedSamples != null) {
                                          selectedSamples.remove(item);
                                          print('Removed sample $item');
                                        } else {
                                          selectedSamples.add(item);
                                          print('Added measure $item');
                                        }
                                      } else {
                                        selectedSamples.remove(item);
                                        print('Removed sample $item');
                                      }
                                    });
                                    print('Selected '
                                        'sample:$sample');
                                  }),
                            )
                            .toList(),
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: Text('Save'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
//                        Text('Select All'),
//                        FlatButton(
//                          child: Text('Clear'),
//                          onPressed: () {
//                            setState(() {
//                              selectedSamples.clear();
//                            });
//                          },
//                        ),
                        FlatButton(
                          child: Text('Cancel'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ));
  }

//  void openSpeciesSelectedDialog(BuildContext context) {
//    var eventProvider = Provider.of<EventProvider>(context, listen: false);
//    List<String> speciesList = ['${eventProvider.species}'];
//    showDialog(
//        context: context,
//        child: Center(
//          child: Container(
//            height: 300,
//            width: 400,
//            child: Dialog(
//              elevation: 30.0,
//              shape: RoundedRectangleBorder(
//                  borderRadius: BorderRadius.circular(15.0)),
//              child: Column(
//                children: <Widget>[
//                  Container(
//                      padding: EdgeInsets.all(12),
//                      alignment: Alignment.centerLeft,
//                      decoration: BoxDecoration(
//                        color: Colors.grey[300],
//                        borderRadius: new BorderRadius.only(
//                            topLeft: const Radius.circular(15),
//                            topRight: const Radius.circular(15)),
//                      ),
//                      child: Text('Species Selected')),
//                  SizedBox(
//                    height: 20.0,
//                  ),
//                  Flexible(
//                    child: Container(
//                      height: 150,
//                      width: 300,
//                      child: ListView(
//                        children: speciesList
//                            .map(
//                              (speciesItem) => SpeciesWidget(
//                                  species: speciesItem,
//                                  onCheckboxItemChecked: (value) {
//                                    setState(() {
//                                      var item = speciesItem;
//                                      if (value == true) {
//                                        if (selectedSpecies.contains(item) &&
//                                            selectedSpecies != null) {
//                                          selectedSpecies.remove(item);
//                                          print('Removed speciesList $item');
//                                        } else {
//                                          selectedSpecies.add(item);
//                                          print('Added speciesList $item');
//                                        }
//                                      } else {
//                                        selectedSpecies.remove(item);
//                                        print('Removed speciesList $item');
//                                      }
//                                    });
//                                    print('Selected '
//                                        'speciesList:$speciesItem');
//                                  }),
//                            )
//                            .toList(),
//                      ),
//                    ),
//                  ),
//                  Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: ButtonBar(
//                      children: <Widget>[
//                        FlatButton(
//                          child: Text('Save'),
//                          onPressed: () {
//                            Navigator.pop(context);
//                          },
//                        ),
//                        FlatButton(
//                          child: Text('Cancel'),
//                          onPressed: () {
//                            Navigator.pop(context);
//                          },
//                        ),
//                      ],
//                    ),
//                  ),
//                ],
//              ),
//            ),
//          ),
//        ));
//  }

  displayErrorDialog(BuildContext context, String message) {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0)),
            title: Text("Error Message"),
            content: Text(message),
            actions: [
              FlatButton(
                child: Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }
}

class SamplesWidget extends StatefulWidget {
  SamplesWidget(
      {Key key, @required this.sample, @required this.onCheckboxItemChecked})
      : super(key: key);

  final String sample;
  final ValueChanged<bool> onCheckboxItemChecked;

  @override
  State<StatefulWidget> createState() {
    return SamplesWidgetState();
  }
}

class SamplesWidgetState extends State<SamplesWidget> {
  bool defaultValue = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Checkbox(
              value: defaultValue,
              activeColor: Theme.of(context).accentColor,
              onChanged: (bool value) {
                setState(() {
                  defaultValue = value;
                  widget.onCheckboxItemChecked(value);
                });
              }),
          SizedBox(
            width: 5.0,
          ),
          Text(widget.sample),
        ],
      ),
    );
  }
}

class SpeciesWidget extends StatefulWidget {
  SpeciesWidget(
      {Key key, @required this.species, @required this.onCheckboxItemChecked})
      : super(key: key);

  final String species;
  final ValueChanged<bool> onCheckboxItemChecked;

  @override
  State<StatefulWidget> createState() {
    return SpeciesWidgetState();
  }
}

class SpeciesWidgetState extends State<SpeciesWidget> {
  bool defaultValue = false;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Checkbox(
              value: defaultValue,
              activeColor: Theme.of(context).accentColor,
              onChanged: (bool value) {
                setState(() {
                  defaultValue = value;
                  widget.onCheckboxItemChecked(value);
                });
              }),
          SizedBox(
            width: 5.0,
          ),
          Text(widget.species),
        ],
      ),
    );
  }
}
